import numpy as np
import matplotlib.pyplot as plt
from control import dare
from model.Quadcopter import Quadcopter

config = dict()
# Configuration of quad for simulation
config["weight"] = 0.9 # Kg
config["ix"] = 8.1e-03
config["iy"] = 8.1e-03
config["iz"] = 14.2e-03
config["i"] = 104e-06
config["l"] = 0.05
config["d"] = 1.1e-06
config["t"] = 54.2e-06

sampling_time = 0.005 # 5ms
init_state = np.mat(np.zeros((12,))).T
quad = Quadcopter(config, init_state, sampling_time)
Q = np.eye(12)
R = np.eye(4)
