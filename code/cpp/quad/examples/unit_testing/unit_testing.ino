/** Test the Quad library
    @author: AMOUSSOU Z. Kenneth
    @date: 23-02-2019
    @version:
**/
#include <Quad.h>

Quad quad;
// init state
float state[6] = {0, 0, 0, 0, 0, 0};
// set state
float sc[6] = {1, 0, 0, 0, 0, 0};

float m[3][3] = {{10, 0, 0},{0, 1, 0},{0, 10, 1}};

// command
float cmd[4];

void setup(){
    Serial.begin(115200);
    delay(100);
    quad.begin();
    Serial.println("Start...");
    Serial.flush();
}

void loop(){
    // quad.update(sc);
    // quad.getCommand(cmd);
    quad.updateState();
    delay(300);
}
