""" Quadcopter model
    @author: AMOUSSOU Z. Kenneth
    @date: 03-02-2019
    @version:
    @dependency:
        numpy:
            [version]: 1.12.1
        matplotlib:
            [version]: 2.0.2
        
"""
import numpy as np
from matplotlib import __version__ as mplt_v
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
from control.matlab import dare

class Quadcopter:
    """ Quadcopter class
        
        :attr state:
            vector that define the state of the quadcopter
            
            x
            y
            z
            x_dot
            y_dot
            z_dot
            phi
            theta
            psi
            phi_dot
            theta_dot
            psi_dot
        
        :attr action:
            vector that define the actions
            
            u1  
            u2 
            u3
            u4
    """
    def __init__(self):
       # self.attitude = np.zeros((12,), dtype=np.float)    # state vector
       # self.action = np.ones((4,), dtype=np.float)   # action vector
        self.sampling_time = 0.005
        self.A = np.zeros((12, 12), dtype=np.float)
        self.B = np.zeros((12, 4), dtype=np.float);
        self.m = 1  # mass
        self.Ixx = 8.1e-03
        self.Iyy = 8.1e-03
        self.Izz = 14.2e-03
        self.I = 104e-06
        self.l = 0.05   # lenght from quad to rotor center
        self.d = 1.1e-06  # drag factor
        self.t = 54.2e-06  # thrust factor
        
    def model(self, state: list, action: list) -> list:
        """ Model the quadcopter based on physics equation
        
            The model return the next state of the quad
        """
        #print("Model on build")
        return (np.multiply(self.A, state).sum(1).reshape(12,) 
                + np.multiply(self.B, action).sum(1).reshape(12,))

    def linearize(self, state, action) -> list:
        """ Linearize the quadcopter model around a given state and command
            
            :return: list
                [A]: state matrix
                [B]: command matrix
        """
        a = np.zeros((12, 12))
        b = np.zeros((12, 4))   
        omega = np.sqrt(self.computeSpeed(action))  # to be initialized
        omega[0] = -omega[0]
        omega[2] = -omega[2]
        omega = omega.sum()
        # TODO: compute position by double integrating the acceleration
        a[3][6] = ((np.sin(state[8]) * np.cos(state[6]) - 
                   np.cos(state[8]) * np.sin(state[7]) * np.sin(state[6]))
                    * action[0]/self.m)
        a[3][7] = (np.cos(state[8]) * np.cos(state[7]) * np.cos(state[6]) * 
                    action[0] / self.m)
        a[3][8] = ((np.cos(state[8]) * np.sin(state[6]) - 
                   np.sin(state[8]) * np.sin(state[7]) * np.cos(state[6]))
                    * action[0]/self.m)
        a[4][6] = ((-np.cos(state[8]) * np.cos(state[6]) - 
                   np.sin(state[8]) * np.sin(state[7]) * np.sin(state[6]))
                    * action[0]/self.m)
        a[4][7] = (np.sin(state[8]) * np.cos(state[7]) * np.cos(state[6]) * 
                    action[0] / self.m)
        a[4][8] = ((np.sin(state[8]) * np.sin(state[6]) + 
                   np.cos(state[8]) * np.sin(state[7]) * np.cos(state[6]))
                    * action[0]/self.m)
        a[5][6] = (-np.sin(state[6]) * np.cos(state[7]) * action[0] / self.m)
        a[5][7] = (-np.cos(state[6]) * np.sin(state[7]) * action[0] / self.m)
        
        a[9][10] = (((self.Iyy - self.Izz)/self.Ixx) * state[11] - 
                     (self.I * omega)/self.Ixx)
        a[9][11] = ((self.Iyy - self.Izz)/self.Ixx) * state[10]
        a[10][9] = (((self.Izz - self.Ixx)/self.Iyy) * state[11] - 
                     (self.I * omega)/self.Iyy)
        a[10][11] = ((self.Izz - self.Ixx)/self.Iyy) * state[9]
        a[11][9] = ((self.Ixx - self.Iyy)/self.Izz) * state[9]
        a[11][10] = ((self.Ixx - self.Iyy)/self.Izz) * state[10]
        
        a[0:3,3:6] = self.sampling_time * np.eye(3);
        a[6:9,9:12] = self.sampling_time * np.eye(3);
        a[3:6, 6:9] *= self.sampling_time
        a[9:12, 9:12] *= self.sampling_time
        
        b[3][0] = ((np.sin(state[8]) * np.sin(state[6]) + 
                   np.cos(state[8]) * np.sin(state[7]) * np.cos(state[6]))
                    /self.m)
        b[4][0] = ((-(np.cos(state[8]) * np.sin(state[6])) + 
                   (np.sin(state[8]) * np.sin(state[7]) * np.cos(state[6])))
                    /self.m)
        b[5][0] = (np.cos(state[7]) * np.cos(state[6])) / self.m
        b[9][1] = 1/self.Ixx
        b[10][2] = 1/self.Iyy
        b[11][3] = 1/self.Izz
        
        return [a, b]
    
    def computeSpeed(self, act: list) -> list:
        """ Compute the squared speed of propellers from input (action)
        
            :param act: (action)
        """
        omega2 = np.zeros((4,))
        omega2[0] = ((act[0]/(4*self.t)) - (act[2]/(2*self.t*self.l))- 
                    (act[3]/(4*self.d)))
        omega2[1] = ((act[0]/(4*self.t)) - (act[1]/(2*self.t*self.l)) -
                    (act[3]/(4*self.d)))
        omega2[2] = ((act[0]/(4*self.t)) + (act[2]/(2*self.t*self.l)) -
                    (act[3]/(4*self.d)))
        omega2[3] = ((act[0]/(4*self.t))+(act[1]/(2*self.t*self.l)) -
                    (act[3]/(4*self.d)))
        # print(np.sqrt(omega2))
        return omega2
    
if __name__ == "__main__":
    print("Modeling");
    
    print("Numpy library");
    print("\t [version]: ", end="");
    print(np.__version__);
    
    print("Matplotlib library");
    print("\t [version]: ", end="");
    print(mplt_v);
    
    quad = Quadcopter()    
    def_u = np.zeros((4,))
    def_u[0] = 9.81
    print(def_u)
    x_hat = np.zeros((12,))
    x_hat[2] = 1
    ma, mb = quad.linearize(x_hat, def_u)
    quad.A = ma
    quad.B = mb
    print(ma)
    print(mb)
    phi = []
    theta = []
    psi = []
    x = []
    y = []
    z = []
    st = np.zeros((12,))
    print("Initial state: ")
    print(st)
    
    # Simulation
    T = 1000 # simulation time
    act = np.array([110, 2, 0, 0])
    for i in range(T):
        currentState = quad.model(st, act)
        phi += [currentState[6]]
        theta += [currentState[7]]
        psi += [currentState[8]]
        x += [currentState[0]]
        y += [currentState[1]]
        z += [currentState[2]]
        st = currentState
    """
    t = [i for i in range(T)]

    fig = plt.figure(1)
    # prepare the grid
    grid = plt.GridSpec(2, 3, wspace=0.4, hspace=0.3)
    
    phi_plot = fig.add_subplot(grid[0, 0])
    theta_plot = fig.add_subplot(grid[0, 1])
    psi_plot = fig.add_subplot(grid[0, 2])
    
    x_plot = fig.add_subplot(grid[1, 0])
    y_plot = fig.add_subplot(grid[1, 1])
    z_plot = fig.add_subplot(grid[1, 2])
    
    phi_plot.plot(t, phi)
    phi_plot.set_title("Phi")
    phi_plot.set_xlabel("Time")
    phi_plot.set_ylabel("Angle (rad)")
    
    theta_plot.plot(t, theta)
    theta_plot.set_title("Theta")
    theta_plot.set_xlabel("Time")
    theta_plot.set_ylabel("Angle (rad)")
    
    psi_plot.plot(t, psi)
    psi_plot.set_title("Psi")
    psi_plot.set_xlabel("Time")
    psi_plot.set_ylabel("Angle (rad)")
    
    x_plot.plot(t, x)
    x_plot.set_title("X")
    x_plot.set_xlabel("Time")
    x_plot.set_ylabel("Distance (m)")
    
    y_plot.plot(t, y)
    y_plot.set_title("Y")
    y_plot.set_xlabel("Time")
    y_plot.set_ylabel("Distance (m)")
    
    z_plot.plot(t, z)
    z_plot.set_title("Z")
    z_plot.set_xlabel("Time")
    z_plot.set_ylabel("Distance (m)")
    
    fig2 = plt.figure(2)
    ax = plt.axes(projection="3d")
    ax.plot3D(x, y, z)
    plt.show()
    """
    Q = np.eye(12)
    R = np.eye(12)
    X, L, G = dare(quad.A, quad.B, Q, R)
    print(G)
    K = (1/R) * np.transpose(quad.B) * X
    print(K)
