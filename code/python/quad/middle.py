""" Pre-computation
    computation of some constant part of the quad model in advance
    @author: AMOUSSOU Z. Kenneth
    @date: 22-02-2018
    @version: 
"""
from model.propeller import Propeller
from model.config import prop_config
from model.config import env
import numpy as np

if __name__ == "__main__":
    prop = Propeller(prop_config, env)
    # configurations
    config = dict();
    config["l"] = 0.105         # m
    config["ix"] = 1.079e-03    # kg.m²
    config["iy"] = 1.079e-03    # kg.m²
    config["iz"] = 2.007e-03    # kg.m²
    config["i"] = 6.3604e-06    # kg.m²
    config["rho"] = prop._rho   # air density
    config["b"] = prop.getB()
    config["d"] = prop.getD()
    
    print("b: ", config["b"])
    print("d: ", config["d"])
    
    # B matrix (continuous time)
    
    
    
    
