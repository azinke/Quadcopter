#
#    @summary: Extract data out of 'data.txt' file
#    @author: AMOUSSOU Zinsou Kenneth
#    @date: 24-01-2019 
#

def process(filename: str, output = "", nb_feature = 2) -> bool:
    # TODO: handle file not found exception - Just raised exception
    if nb_feature < 0: return False
    if output == "": output = "processed.txt"
    with open(output, "w") as phandle:
        with open(filename, "r") as fhandle:
            for line in fhandle:
                if line.strip() == "": continue
                _buffer = line.split(",")
                # Scale command between 0 and 1
                _buffer[0] = int(_buffer[0])/2000 # define 2000 as max command
                # convert speed in rpm
                _buffer[1] = int(_buffer[1])*60
                phandle.write(str(_buffer[0]) + " ")
                i = 2
                while (i <= nb_feature):
                    phandle.write(str(_buffer[0]**i) + " ")
                    i = i + 1
                phandle.write(str(_buffer[1]))
                phandle.write("\n")
                                
#               print("cmd: {0} - speed: {1} tr/s".format(_buffer[0], 
#                                                         _buffer[1]))
    return True

if __name__ == "__main__":
    if  process("../data.txt", "processed.txt", 1):
        print("Success!")
    else:
        print("Failed!")
