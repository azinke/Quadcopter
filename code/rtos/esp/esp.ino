#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <WebSocketsServer.h>
#include <Servo.h>
#include "index.h"

#define WEB_SERVER_PORT         80
#define WEBSOCKET_SERVER_PORT   81

#define TX1 3   // RX
#define TX2 1   // TX

Servo channelOne;
Servo channelTwo;

bool channel_one_upper = false;
bool channel_one_lower = false;

// WiFi parameters
const char* ssid = "bee";
const char* password = "queenbee";

IPAddress local_IP(192,168,4,2);
IPAddress gateway(192,168,4,1);
IPAddress subnet(255,255,255,0);


ESP8266WebServer server(WEB_SERVER_PORT);
WebSocketsServer webSocket = WebSocketsServer(WEBSOCKET_SERVER_PORT);

/* @var command: hold the remote pilote commands
 *      char at:
 *      - [0]: on off command
 *      - [1]: automatic take off command
 *      - [2]: automatic landing command
 *      - [3]: throttle up command
 *      - [4]: throttle down command
 *      - [5]: move right command
 *      - [6]: move left command
 *      - [7]: forward movement command
 *      - [8]: backward movement command
 *      - [9]: yaw right mouvement command
 *      - [10]: yaw left mouvement command
 */
String command = "00000000000";
uint8_t strln_max = 10;

void setup(){
    // switch RX and TX pin as GPIO pin
    // to switch them back to serial communication mode
    // set pinMode(rx | tx, FUNCTION_0)
    pinMode(TX1, FUNCTION_3);
    pinMode(TX2, FUNCTION_3);

    channelOne.attach(TX1);
    channelTwo.attach(TX2);
    
    // default off state
    channelOne.writeMicroseconds(1050);
    channelTwo.writeMicroseconds(1050);

    /* Set module parameter */
    WiFi.softAPConfig(local_IP, gateway, subnet);
  
    WiFi.mode(WIFI_AP_STA);
    WiFi.softAP(ssid, password);

    server.on("/", [](){
        server.send(200, "text/html", html);
    });

    // WebSocket server
    webSocket.begin();
    webSocket.onEvent(webSocketEvent);
    // Webserver 
    server.begin();
    // DNS server
    MDNS.begin("bee");
}

void loop(){
    // handle websocket client request
    webSocket.loop();
    // handle web client request
    server.handleClient();
}

/**
    @callback: webSocketEvent
    @summary: handle websocket event's
    @parameters:
        num: client number
        type: the type of the event
            [WStype_DISCONNECTED]   : client disconnected
            [WStype_CONNECTED]      : client connected
            [WStype_TEXT]           : text data received
            [WStype_BIN]            : binary data received
        payload: data
        length: the lenght (size) of the payload (data) 
    @return: none
**/
void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length) {
    switch(type) {
        case WStype_DISCONNECTED:
            break;
        case WStype_CONNECTED: {
            // IPAddress ip = webSocket.remoteIP(num);
            // ip[0], ip[1], ip[2], ip[3] : to each segment of IP address
            // payload: contain the url of the client
	
	        // send message to client
	        //webSocket.sendTXT(num, "message here");
            break;
        }
        case WStype_TEXT: {
            // send message to client
            // webSocket.sendTXT(num, "message here");

            // send data to all connected clients
            // webSocket.broadcastTXT("message here");
            
            // Commands
            // read and parse the client data
            //command = String((char*)payload);
            
            command = String((char*)payload);
            channel_one_upper = false;
            channel_one_lower = false;
            // start the quad
            if(command.charAt(0) == '1'){
                //
                // Upper channel
                //
                
                // takeoff
                if(command.charAt(1) == '1') { 
                    channelTwo.writeMicroseconds(1150);
                    channel_one_upper = true;
                }
                // up
                if(command.charAt(3) == '1'){
                    channelTwo.writeMicroseconds(1250);
                    channel_one_upper = true;
                }
                // right
                if(command.charAt(5) == '1'){
                    channelTwo.writeMicroseconds(1350);
                    channel_one_upper = true;
                }
                // forward
                if(command.charAt(7) == '1'){
                    channelTwo.writeMicroseconds(1450);
                    channel_one_upper = true;
                }
                // yaw right
                if(command.charAt(9) == '1'){
                    channelTwo.writeMicroseconds(1550);
                    channel_one_upper = true;
                }
                
                //
                // Lower channel
                //
                
                // landing
                if(command.charAt(2) == '1'){
                    channelTwo.writeMicroseconds(1150);
                    channel_one_lower = true;
                }
                
                // down
                if(command.charAt(4) == '1'){
                    channelTwo.writeMicroseconds(1250);
                    channel_one_lower = true;
                }
                
                // backward
                if(command.charAt(6) == '1'){
                    channelTwo.writeMicroseconds(1350);
                    channel_one_lower = true;
                }
                
                // left
                if(command.charAt(8) == '1'){
                    channelTwo.writeMicroseconds(1450);
                    channel_one_lower = true;
                }
                
                // yaw_left
                if(command.charAt(10) == '1'){
                    channelTwo.writeMicroseconds(1550);
                    channel_one_lower = true;
                }
                
                // update channel one
                if(channel_one_upper) channelOne.writeMicroseconds(1650);
                // update channel two
                else if(channel_one_lower) channelOne.writeMicroseconds(1350);
                // standby mode
                // waiting for a command
                else channelOne.writeMicroseconds(1150);
            }
            // halt the quad
            else channelOne.writeMicroseconds(1050);
            // prepare data to send to client
            break;
        }
        case WStype_BIN: {
           // read binary data
           // hexdump(payload, length);
            // command = String((char*)payload);
            // webSocket.sendTXT(num, "ACK");
            // send message to client
            // webSocket.sendBIN(num, payload, length);
            break;
        }
    }
}
