# Motor control model

In order to find the motor's effective control model, a set of program have been 
written.

- The Arduino program located at `./speedMeter/speedMeter.ino` hold the program
to collect the required data for the modeling purpose. Actually, the pulse width 
and the motor's speed are recorded.

- The data collected are save in the file: `./data.txt`

- The datas are pre-processed by the python script `./py/build.py` and saved in 
the file `./py/processed.txt`

- Then, the effective control model of the motor is derived by applying machine 
learning with linear-regression algorithm. The program that handle this is 
written in `matlab scripting laguage`. See the file : 
`./linear-regression/linear_regression.m`

- Some ploting tasks have been also done in the program file: `./py/ploting.py`
