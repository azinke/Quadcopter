""" Attitude model
    @author: AMOUSSOU Z. Kenneth
    @date: 07-02-2019
    @update: 18-02-2019
"""
import numpy as np
import matplotlib.pyplot as plt
from control import dare

class Quadcopter:
    """ Quadcopter simulator 
        
        Attitude controller design for the quad copter
    """

    def __init__(self):
        self.sampling_time = 0.004
        self.ix = 1.079e-03
        self.iy = 1.079e-03
        self.iz = 2.007e-03
        self.A = np.mat(np.eye(6, dtype="float"))
        self.B = np.mat(np.zeros((6, 3), dtype="float"))
        self.A[0:3,3:7] = self.sampling_time * np.mat(np.eye(3))
        self.B[3,0] = self.sampling_time/self.ix
        self.B[4,1] = self.sampling_time/self.iy
        self.B[5,2] = self.sampling_time/self.iz
        self.d = 1.752e-08
        self.t = 1.461e-06
        self.l = 0.105


    def model(self, state: list, action: list) -> list:
        """ Compute the next state of the quad given the current one
            :param state: 1x6 matrix
                [0]: roll
                [1]: pitch
                [2]: yaw
                [3]: roll angular rate
                [4]: pitch angular rate
                [5]: yaw angular rate
            :param action: 1x3 matrix
                [0]: roll moment
                [1]: pitch moment
                [2]: yaw moment
        """
        # state = np.mat(state)
        # action = np.mat(action)
        return self.A * state + self.B * action
    
    def speed(self, action: list) -> list:
        p1 = 1/(4*self.t)
        p2 = np.sqrt(2)/(4*self.t*self.l)
        p3 = 1/(4*self.d)
        o2 = np.mat(np.zeros((4,)))
        o2[0] += action[0] * p1
        o2[0] += action[1] * p2
        o2[0] -= action[2] * p2
        o2[0] -= action[3] * p3
        
        o2[1] += action[0] * p1
        o2[1] -= action[1] * p2
        o2[1] -= action[2] * p2
        o2[1] += action[3] * p3
        
        o2[2] += action[0] * p1
        o2[2] -= action[1] * p2
        o2[2] += action[2] * p2
        o2[2] -= action[3] * p3
        
        o2[3] += action[0] * p1
        o2[3] += action[1] * p2
        o2[3] += action[2] * p2
        o2[3] += action[3] * p3
        # print(np.sqrt(o2))
        return o2
    
    def computeReward(self, state: list, action: list, Q: list, R: list) -> int:
        """ Compute the cost (reward) based on the current state and action
            
            :note: parameters shoud be matrices type
            :param state: state vector
            :param action: action vector
            :parma Q: lqr Q matrix coefficient
            :param R: lqr R matrix coefficient
        """
        return -((state.T*Q*state)+(action.T*R*action))
        
    def learn(self, Q: list, R: list, T: int) -> list:
        """ Learning procedure to obtain the gain for control
            :param Q: state ponderation coefficient
            :param R: action ponderation coefficent
            :param T: Time horizon
        """
        Ut = np.mat(Q)
        Wt = np.mat(R)
        state = np.mat(100*np.random.random((6,))).T
        action = np.mat(np.zeros((3,))).T
        # Init
        Vt = self.computeReward(state, action, Ut, Wt)
        Pt = - Ut
        Psi_t = 0
        t = T
        vlogs = []
        rlogs = []
        while t > 0:
            Vt = Vt + self.computeReward(state, action, Ut, Wt)
            Pt = Q + self.A.T*Pt*self.A - (self.A.T*Pt*self.B)*(self.B.T*Pt*self.B).I*(self.B.T*Pt*self.A)
            Lt = - (Wt+self.B.T*Pt*self.B).I*self.B.T*Pt*self.A
            action = Lt * state
            state = self.model(state, action);
            vlogs.append(Vt.sum());
            t = t - 1
        # figure = plt.figure(1)
        # t = [i for i in range(T)]
        # plt.plot(t, vlogs)
        # plt.show()
        return Lt
        
    def learnx(self, trajectory: list, Q: list, R: list, T: int) -> list:
        """ Learning procedure to obtain the gain for control in order to follow
            a given trajectory
            :param trajectory: set trajectory to follow
            :param Q: state ponderation coefficient
            :param R: action ponderation coefficent
            :param T: Time horizon
        """
        Ut = np.mat(Q)
        Wt = np.mat(R)
        state = np.mat(100*np.random.random((6,))).T
        action = np.mat(np.zeros((3,))).T
        # Init
        Vt = self.computeReward(state, action, Ut, Wt)
        Pt = - Ut
        es = np.mat(np.zeros((6,))).T
        t = T
        vlogs = []
        while t > 0:
            Vt = Vt + self.computeReward(state, action, Ut, Wt)
            Pt = Ut + self.A.T*Pt*self.A - (self.A.T*Pt*self.B)*(self.B.T*Pt*self.B).I*(self.B.T*Pt*self.A)
            Lt = - (Wt+self.B.T*Pt*self.B).I*self.B.T*Pt*self.A
            Ft = (self.A-self.B*Wt.I*self.B.T*Pt).T
            es = Ft*es - Ut*trajectory
            Ot = - Wt.I*self.B.T
            action = Lt * state + Ot * es
            state = self.model(state, action);
            # vlogs.append(Vt.sum());
            t = t - 1
        # figure = plt.figure(1)
        # t = [i for i in range(T)]
        # plt.plot(t, vlogs)
        # plt.show()
        return [Lt, Ot, es]



if __name__ == "__main__":
    print(" Attitude simulator ")
    # Time
    T = 500
    quad = Quadcopter()
    # angular position
    phi = []
    theta = []
    psi = []
    # action - momentum around x, z and z axis
    mx = []
    my = []
    mz = []
    
    Reward = []
    Q = np.eye(6)
    # default: 0.025
    Q[0][0] = 10
    Q[1][1] = 10
    Q[2][2] = 1
    R = 500 * np.eye(3) # 625
    lt = quad.learn(Q, R, T)
    print("Gain: ");
    print(lt)
    
    # trajectory = np.mat([1, 0, 0, 0, 0, 0]).T
    # [lt, ot, es] = quad.learnx(trajectory, Q, R, T)
    
    # Random initial state
    st = np.mat(np.random.random((6,))).T
    st[3] = 0
    st[4] = 0
    st[5] = 0
    print("init state: ")
    print(st)
    for i in range(T):
        act = lt * st
        next_state = quad.model(st, act)
        phi.append(next_state[0].sum())
        theta.append(next_state[1].sum())
        psi.append(next_state[2].sum())
        mx.append(act[0].sum())
        my.append(act[1].sum())
        mz.append(act[2].sum())
        st = next_state
        #Reward += 
#    phi = [x * 0.003 for x in phi]
#    theta = [x * 0.003 for x in theta]
#    psi = [x * 0.003 for x in psi]
    #time base

    t = [i for i in range(T)]
    # plot grah
    fig = plt.figure(1)
    grid = plt.GridSpec(2, 3, wspace=0.5, hspace=0.5)
    phi_plt = fig.add_subplot(grid[0, 0])
    theta_plt = fig.add_subplot(grid[0, 1])
    psi_plt = fig.add_subplot(grid[0, 2])
    # momentum plots
    mx_plt = fig.add_subplot(grid[1, 0])
    my_plt = fig.add_subplot(grid[1, 1])
    mz_plt = fig.add_subplot(grid[1, 2])

    phi_plt.plot(t, phi)
    phi_plt.set_title("Phi")
    phi_plt.set_ylabel("rad")

    theta_plt.plot(t, theta)
    theta_plt.set_title("Theta")
    theta_plt.set_ylabel("rad")

    psi_plt.plot(t, psi)
    psi_plt.set_title("Psi")
    psi_plt.set_ylabel("rad")
    
    mx_plt.plot(t, mx)
    mx_plt.set_title("mx")
    mx_plt.set_ylabel("kg m²")
    
    my_plt.plot(t, my)
    my_plt.set_title("my")
    my_plt.set_ylabel("kg m²")
    
    mz_plt.plot(t, mz)
    mz_plt.set_title("mz")
    mz_plt.set_ylabel("kg m²")

    plt.show()
    
    
