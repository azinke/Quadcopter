""" Attitude controller
    
    @author: AMOUSSOU Z. Kenneth
    @date: 11-02-2019
    @version:
"""
import numpy as np
import matplotlib.pyplot as plt
from control import dare

class Attitude:
    """ Attitude controller of the quad
    """
    
    def __init__(self, config: dict, init_state: list, sampling_time: int):
        """ Constructor
            :param config:
                ["weight"]  : quad's weight
                ["ix"]      : inertia momentum around x-axis
                ["iy"]      : inertia momentum around y-axis
                ["iz"]      : inertia momentum around z-axis
                ["i"]       : inertia momentum
                ["l"]       : lenght from quad to motor center
                ["d"]       : propeller's drag coefficient
                ["t"]       : propeller's thrust coefficient
            :param init_state: initial state of the quad as a column vector
            :param sampling_time: sampling time for discret modeling
        """
        self._st = sampling_time                # sampling time
        self._A = np.mat(np.zeros((6, 6)))    # state matrix
        self._B = np.mat(np.zeros((6, 4)))     # control matrix
        try:
            self._w = config["weight"]
            self._i = config["i"]
            self._ix = config["ix"]
            self._iy = config["iy"]
            self._iz = config["iz"]
            self._d = config["d"]
            self._t = config["t"]
            self._l = config["l"]
        except:
            print("An error occur during configuration")
            sys.exit(0)
        for i in range(3):
            self._A[2*i:2*i+2,2*i:2*i+2] = np.mat([[1, self._st], [0, 1]])
        init_action = np.mat([self._w*9.81, 0, 0, 0]).T
        omega = np.sqrt(self.actionToSpeed(init_action))
        omega[0] = -omega[0]
        omega[2] = -omega[2]
        omega = omega.sum()
        self._A[1, 3] = -(self._st*self._i/self._ix)*omega
        self._A[3, 1] = -(self._st*self._i/self._iy)*omega
        self._B[1, 1] = self._st/self._ix
        self._B[3, 2] = self._st/self._iy
        self._B[5, 3] = self._st/self._iz
        print(self._A)
    
    def model(self, state: list, action: list) -> list:
        """ core of the simulator
            :param state: current state of the quad as a column vector
            :param action: current action taken as a column vector
            :return: the next state of the quad given the current state and 
            action
            
            X(t+1) = [A]X(t) + [B]u
        """
        return (self._A * state) + (self._B * action)
    
    def simulate(self, state: list, action: list, time:int) -> list:
        """ Run the model for a certain time and log it's state over time
            :param state: initial state to start the similation
            :param action: action to perform during the simulation
            :param time: duration of the simulation in number of time step
        """
        logs = []
        current_state = state
        for i in range(time):
            self.update(current_state, action)
            next_state = self.model(current_state, action)
            logs.append(next_state)
            current_state = next_state
        return logs         
    
    def actionToSpeed(self, action: list) -> list:
        """ Compute the speed of each motor of the quad given it's current set
            of action
            :param action: current action
        """
        omega2 = np.zeros((4,))
        omega2[0] = ((action[0]/(4*self._t)) - (action[2]/(2*self._t*self._l)) - 
                    (action[3]/(4*self._d)))
        omega2[1] = ((action[0]/(4*self._t)) - (action[1]/(2*self._t*self._l)) +
                    (action[3]/(4*self._d)))
        omega2[2] = ((action[0]/(4*self._t)) + (action[2]/(2*self._t*self._l)) -
                    (action[3]/(4*self._d)))
        omega2[3] = ((action[0]/(4*self._t))+(action[1]/(2*self._t*self._l)) +
                    (action[3]/(4*self._d)))
        return omega2
    
    def getReward(self, state: list, action: list, Q: list, R: list) -> int:
        """ Compute the reward for a given state and action    
            :param state: current state
            :param action: action being performed
            :param Q: Q matrix of LQR - constant matrix
            :param R: R matrix of LQR - constant matrix    
            
            
        """
        return - (state.T*Q*state) - (action.T*R*action)
    
    def learn(self, Q: list, R: list, time:int) -> list:
        """ LQR reinforcement learning
        """
        logs = []
        Vt = 0
        Pt = Q
        # state = np.mat(10*np.random.random((6,))).T
        state = np.mat([-0.08, 0, 0.01, 0, 6.2, 0]).T
        action = np.mat(10*np.random.random((4,))).T
        for i in range(time):
            Vt = self.getReward(state, action, Q, R)# + Vt
            Lt = -(R+self._B.T*Pt*self._B).I*self._B.T*Pt*self._A
            action = Lt*state
            state = self.model(state, action) # compute next state
            # Pt = Q+self._A.T*Pt*self._A-(self._A.T*Pt*self._B*(self._B.T*Pt*self._B+R).I*self._B.T*Pt*self._A)
            Pt = Q+self._A.T*Pt*self._A-(self._A.T*Pt*self._B*(-Lt))
            logs.append([Vt, state])
        # print(Lt)
        return logs
    
    def learnx(self, setTraj:list, Q: list, R: list, time:int) -> list:
        """ LQR reinforcement learning
        """
        logs = []
        Vt = 0
        e = np.mat(np.zeros((6,))).T
        Pt = -Q
        # state = np.mat(10*np.random.random((6,))).T
        state = np.mat([0, 0, 0, 0, 0, 0]).T
        action = np.mat(10*np.random.random((4,))).T
        for i in range(time):
            Vt = self.getReward(state, action, Q, R)# + Vt
            Lt = -(R+self._B.T*Pt*self._B).I*self._B.T*Pt*self._A
            state = self.model(state, action) # compute next state
            Pt = Q+self._A.T*Pt*self._A-(self._A.T*Pt*self._B*(self._B.T*Pt*self._B+R).I*self._B.T*Pt*self._A)
            # Pt = Q+self._A.T*Pt*self._A-(self._A.T*Pt*self._B*(-Lt))
            e = (self._A-(self._B*R.I*self._B.T*Pt)).T*e - Q*setTraj
            action = Lt*state - R*self._B.T*e
            logs.append([Vt, state])
            print("Pt: ", Pt)
            print("Lt: ", Lt)
            print("e: ", e)
        # print(Lt)
        return logs

if __name__ == "__main__":
    config = dict()
    # Configuration of quad for simulation
    config["weight"] = 0.9 # Kg
    config["ix"] = 8.1e-03
    config["iy"] = 8.1e-03
    config["iz"] = 14.2e-03
    config["i"] = 104e-06
    config["l"] = 0.05
    config["d"] = 1.1e-06
    config["t"] = 54.2e-06
    
    sampling_time = 0.012 # 5ms
    simulation_time = 50  # ~ 5ms*500 = 2500ms
    
    init_state = np.mat([-0.08, 0.01, 6.2, 0, 0, 0]).T
    
    quad = Attitude(config, init_state, sampling_time)
    Q = np.mat(np.eye(6))
    Q[0,0] = 200
    Q[2,2] = 200
    Q[4,4] = 200
    R = np.mat(np.eye(4))
    
    R[1,1] = 10
    R[2,2] = 10
    R[3,3] = 10
    
    logs = quad.learn(Q, R, simulation_time)
    sc = np.mat([1., 0, 0, 0, 0, 0]).T
    # logs = quad.learnx(sc, Q, R, simulation_time)
    
    v = []
    phi = []
    theta = []
    psi = []
    # retrieve useful data
    for vlog, log in logs:
        v.append(vlog[0].sum())
        phi.append(log[0].sum())
        theta.append(log[2].sum())
        psi.append(log[4].sum())
    # Plot graphs
    grid = plt.GridSpec(2, 3, wspace=0.5, hspace=0.5)
    figure = plt.figure(1)
    vx = figure.add_subplot(grid[0, 0:])
    vx.set_title("Value function")
    phix = figure.add_subplot(grid[1, 0])
    phix.set_title("Phi")
    thetax = figure.add_subplot(grid[1, 1])
    thetax.set_title("Theta")
    psix = figure.add_subplot(grid[1, 2])
    psix.set_title("Psi")
    
    t = [j for j in range(simulation_time)]
    vx.plot(t, v)
    phix.plot(t, phi)
    thetax.plot(t, theta)
    psix.plot(t, psi)
    plt.show()
    
    
    # solution, eigv, gain = dare(quad._A, quad._B, Q, R)
    # print("Gain: ", end="")
    # print(gain)
    # print(-gain*init_state)
