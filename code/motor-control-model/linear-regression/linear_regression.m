% 
%   @summary: Linear regression template
%   @author: AMOUSSOU Z. Kenneth
%   @date: 06-01-2019
%

%
% General data
%
alpha = 0.1;
%
% the number of iteration can influence the learning
%
number_of_iterations = 6000;
%
% Read data
%
% Simulation data
%data = load("../data.txt");
data = load("../py/processed.txt");
X = data(:, 1:(size(data, 2)-1));
Y = data(:, size(data, 2));
size(X)
size(Y)
data_size = size(Y); % update data size

%
% Configurations
%

data_size = size(data, 1);
number_of_features = (size(data, 2)) - 1;


%
% Initialize theta
%
printf("Random initial parameters: \n");
theta = rand(number_of_features + 1, 1)

%
% @function: featureNormalize
% @summary: feature scaling
% @parameters:
%   X: inpute dataset
%   y: output dataset
%   theta: the hypothesis function's parameters vector
%   alpha: learning rate
%   number_of_iterations: how many time to repeat the learning process
%
function [X_norm, mu, sigma] = featureNormalize(X)
    mu = mean(X);
    sigma = std(X);
    X_norm = (X - mu) ./ sigma;
 end

%
% @function: gradientDescent
% @summary: Gradient descent
% @parameters:
%   X: inpute dataset
%   y: output dataset
%   theta: the hypothesis function's parameters vector
%   alpha: learning rate
%   number_of_iterations: how many time to repeat the learning process
%
function [theta, cost_function_logs] = gradientDescent(X, Y, theta, alpha, number_of_iterations)
   idx = 0;
   %data size
   m = size(X)(1);
   cost_function_logs = zeros(number_of_iterations, 1);   
   for idx = 1:number_of_iterations
       error = X * theta - Y;
       grad = (1/m) .* X' * error;
       theta = theta - alpha .* grad;
       cost_function_logs(idx) = computeCost(X, Y, theta);
   end
end

%
% @function: computeCost
% @summary: compute the cost funtion for a single step
% @parameters:
%   X: input training set
%   Y: output training set
%   theta: the hypothesis function's parameters vector
%
function [cost] = computeCost(X, Y, theta)
    m = length(Y);
    error = X * theta - Y;
    cost = (0.5/m) .* error' * error;
end

%[X mu sigma ] = featureNormalize(X);
X = [ones(data_size, 1) X];

[theta, J_logs] = gradientDescent(X, Y, theta, alpha, number_of_iterations);

% print out parameters
printf("Learned parameters: \n");
theta

figure(1)
plot([1:1:number_of_iterations], "markersize", 4, J_logs, "r-")
xlabel("Nombre d'itération")
ylabel("Fonction de cout")

figure(2)
plot(X(:, 2), Y);
xlabel("Commande")
ylabel("Vitesse - RPM")

% Prediction
y1 = X * theta;
hold on;
plot(X(:, 2), y1, "markersize", 4, "r*");

