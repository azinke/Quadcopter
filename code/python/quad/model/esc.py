""" Model an Electronic Speed Controller
    
    @author: AMOUSSOU Z. Kenneth
    @date: 13-02-2019
    @version:
"""
import math
from sys import exit

class ESC:
    """ ESC modeling
        
        The purpose is to be able to estimate the throttle command of the ESC.
        As well as its voltage and current
        :output:
            [1] throttle command
            [2] voltage of the ESC
            [3] current through the ESC
    """
    
    def __init__(self, config: dict):
        """ Constructor
            :param config: configuration data of the ESC
        """
        try:
            self._imax = config["max-current"]
            self._re = config["output-resistor"]
            self._w = config["weight"]
        except:
            print("Missing modeling data of the ESC!")
            exit(0)
    
    def getCommand(self, vmotor: float, imotor: float, vbat: float) -> float:
        """ compute the throttle command to send to the ESC based on the motor 
            voltage, the motor current and the battery voltage
            :param vmotor: voltage across the motor
            :param imotor: current through the motor
            :param vbat: voltage across the battery
            :return: the throttle command
        """
        return ((vmotor + (self._re*imotor))/vbat)
    
    def getCurrent(self, vmotor: float, imotor: float, vbat: float) -> float:
        """ compute the current delivered by the esc
            :param vmotor: voltage across the motor
            :param imotor: current through the motor
            :param vbat: voltage across the battery
            :return: ESC's output current
        """
        return (self.getCommand(vmotor, imotor, vbat) * imotor)
    
    def getVoltage(self, vbat: float, ibat: float, rbat: float) -> float:
        """ compute the voltage across the ESC
            :param vbat: voltage across the battery
            :param ibat: the current delivered by the battery
            :param rbat: the internal resistor of the battery
            :return: ESC's output voltage
        """
        return (vbat - (rbat*ibat))

if __name__ == "__main__":
    # configuration of the ESP
    config = {
        "max-current": 16,          # ampere
        "output-resistor": 0.01,    # ohm
        "weight": 6                 # gram
    }
    
    esc = ESC(config)
    vmotor = 4
    imotor = 1.2
    vbat = 12
    print("ESC Command(vmotor: {0}V, imotor: {1}A, vbat: {2}V): {3}".format(
            vmotor, imotor, vbat, esc.getCommand(vmotor, imotor, vbat)))
