/** Quadcopter modeling
 * @author: AMOUSSOU Z. Kenneth
 * @date: 22-02-2019
 * @version: 
 */
#ifndef QUAD_H
#define QUAD_H

#include <Arduino.h>
#include <bmp180.h>
#include <BNO055.h>
#include <PID.h>

#define QUAD_DEBUG 1
#define QUAD_PROFILING 0

#define IMU     // to enable or disable IMU sensor
#define BMP     // to enable or disable BMP sensor

#define MAX_MOMENT  0.02
#define MAX_THRUST  10.89

// read the thesis for more detail
// variables are most likely used to match the thesis

class Quad{
    public:
        // constructor
        Quad();

        // initialization
        void begin();

        // re-initialize for next horizon
        void reset();

        // read the pwm signal to send to each esc
        void getCommand(float *cmd);    // get the command (range: 0 - 1)

        // compute the weighted sum of the speed of all propellers
        // omega = - omega1  + omega2 - omega3 + omega4
        float getOmega();       

        // Compute the new action that should be performed
        void update(float *sc);
        
        // update the motor's speed based on chosen action
        // (from control vector)
        void updateSpeed();

        // update the quad state vector 
        // This should be done as often as possible
        void updateState();    
        
        // read the quad's attitude
        void readAttitude(float *state);
        
        // print matrix for debugging
        void debug(float *m, uint8_t r, uint8_t c, String text);
        
        // print quad euler angles
        void printAngles();
        
        float getUx0();
        void setUx0(float value);
    private:
        float l;    // arm lenght
        float ix;   // moment of inertia around the x-axis
        float iy;   // moment of inertia around the y-axis
        float iz;   // moment of inertia around the z-axis
        float i;    // moment of inertia of motor+propeller
        float b;    // thrust coefficient
        float d;    // torque coefficient
        float w;    // weight
        uint16_t horizon;

        // The attitude controller should be executed ten times faster than
        // others controllers
        float st;   // sampling time
        float alt_st;   // altitude sampling time

        float A[4][4]; // state matrix

        float B[4][2]; // control matrix
        float Bt[2][4]; // transpose of the control matrix

        float ux[4];    // control vector - action
        float speed[4];    // speed vector of the propellers

        // Control matrix
        float Q[4][4];
        float R[2][2];

        float Pt[4][4];
        float Lt[2][4];
        float Ot[2][4];
        float Es[4];   // column vector

        // used to hold temporary data to avoid computing the same
        // formula twice in one loop
        float ans0[2][2];
        float ans1[2][4];
        float ans2[2][4];
        float ans3[2][2];
        float ans4[4][4];
        float ans5[4][4];
        float ans6[4][2];
        
        float tmpv1[4];
        float tmpv2[4];
        
        float tmpv3[2];
        float tmpv4[2];
        // 
        // control matrix element
        //
        float coef1;
        float coef2;
        float coef3;
        float p1;
        float p2;
        float p3;
        float p4;

        float quad_state[4];
        float quad_altitude;
};

#endif
