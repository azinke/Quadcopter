/** Matrix library
    
    Matrix calculus
    @author: AMOUSSOU Z. Kenneth
    @date: 19-02-2019
    @version:
*/
#ifndef MAT_H
#define MAT_H
#include <stdint.h>
#include <math.h>

/**
    @function: _add
    @summary: add two matrices
    @param a: input matrix
    @param b: input matrix
    @param ans: result
    @param r: row size
    @param c: column size
    @return : none
*/
void _add(float *a, float *b, float *ans, uint8_t r, uint8_t c ){
    unsigned int i = 0;
    unsigned int j = 0;
    for(i = 0; i < r; i++){
        for(j = 0; j < c; j++){
            ans[i*c+j] = a[i*c+j]+b[i*c+j];
        }
    }
}

/**
    @function: _sub
    @summary: substract two matrices
    @param a: input matrix
    @param b: input matrix
    @param ans: result
    @param r: row size
    @param c: column size
    @return : none
*/
void _sub(float *a, float *b, float *ans, uint8_t r, uint8_t c ){
    unsigned int i = 0;
    unsigned int j = 0;
    for(i = 0; i < r; i++){
        for(j = 0; j < c; j++){
            ans[i*c+j] = a[i*c+j] - b[i*c+j];
        }
    }
}

/**
    @function: _eye
    @summary: create an identity matrix
    @param ans: result
    @param size: row size
    @return : none
*/
void _eye(float *ans, uint8_t size){
    uint8_t i = 0;
    for(i = 0; i < size; i++) ans[i*size+i] = 1;
}

/**
    @function: _null
    @summary: create a null matrix
    @param ans: result
    @param r: row size
    @param c: column size
    @return : none
*/
void _null(float *ans, uint8_t r, uint8_t c){
    uint8_t i = 0;
    uint8_t j = 0;
    for(i = 0; i < r; i++)
        for(j = 0; j < c; j++)
            ans[i*c+j] = 0;
}

/**
    @function: _scale
    @summary: multiply a matrix by a constant
    @param a: input matrix
    @param ans: result
    @param r: row size
    @param c: column size
    @return : none
*/
void _scale(float *a, float coef, float *ans, uint8_t r, uint8_t c){
    uint8_t i = 0;
    uint8_t j = 0;
    for(i = 0; i < r; i++)
        for(j = 0; j < c; j++)
            ans[i*c+j] = a[i*c+j] * coef;
}

/**
    @function: _copy
    @summary: make a copy of a matrix given an adress
    @param a: original matrix
    @param ans: the copy
    @param r: row size
    @param c: column size
    @return : none
*/
void _copy(float *a, float *ans, uint8_t r, uint8_t c){
    uint8_t i = 0;
    uint8_t j = 0;
    for(i = 0; i < r; i++)
        for(j = 0; j < c; j++)
            ans[i*c+j] = a[i*c+j];
}

/**
    @function: _mul
    @summary: multiply two matrices
    @param a: input matrix
    @param b: input matrix
    @param ans: result
    @param r: row size  (first matrix)
    @param c: column size (first matrix)
    @param q: column size (second matrix)
    @return : none
    
    @note: the column of the first matrix should equal the number of row in
    the second
*/
void _mul(float *a, float *b, float *ans, uint8_t r, uint8_t c, uint8_t q){
    uint8_t i = 0;
    uint8_t j = 0;
    uint8_t k = 0;   
    for(k = 0; k < q ; k++){
        for(i = 0; i < r; i++){
            ans[i*q+k] = 0;
            for(j = 0; j < c; j++){
                ans[i*q+k] += a[i*c+j] * b[j*q+k];
            }
        }
    }
}

/**
    @function: _mulv
    @summary: multiply a matric by a vector column
    @param a: input matrix
    @param b: input vector
    @param ans: result
    @param r: row size  (first matrix)
    @param c: column size (first matrix)
    @return : none
    
    @note: the column of the first matrix should equal the number of row in
    the second
*/
void _mulv(float *a, float *b, float *ans, uint8_t r, uint8_t c){
    uint8_t i = 0;
    uint8_t j = 0;
    for(i = 0; i < r; i++){
        ans[i] = 0;
        for(j = 0; j < c; j++){
            ans[i] += a[i*c+j] * b[j];
        }
    }
}

/**
    @function: _lup
    @summary: LUP decomposition of a matrix
    @param a: input matrix
    @param p: vector containing permutations
    @param size: the matrix size
    @return : none
    
    @note 1: matrix a contains LU elements
    @note 2: should be square matrix
*/
uint8_t _lup(float *a, int *p, uint8_t size){
    uint8_t v = 0;
    uint8_t tmp = 0;
    float buffer = 0;
    // init permutation vector
    for(uint8_t i = 0; i < size; i++)
        p[i] = i;
    for(uint8_t k = 0; k < size; k++){
        uint8_t x = 0;
        for(uint8_t i = k; i < size; i++){
            if(fabs(a[i*size+k]) > x){
                x = fabs(a[i*size+k]);
                v = i;
            }
        }
        if(x == 0) return 0; // singular matrix
        tmp = p[v];
        p[v] = p[k];
        p[k] = tmp;
        for(uint8_t i = 0; i < size; i++){
            buffer = a[k*size+i];
            a[k*size+i] = a[v*size+i];
            a[v*size+i] = buffer;
        }
        for(uint8_t i = k+1; i < size; i++){
            a[i*size+k] = a[i*size+k]/a[k*size+k];
            for(uint8_t j = k+1; j < size; j++)
                a[i*size+j] -= a[i*size+k]*a[k*size+j];
        }
    }
    return 1;
}

/**
    @function: _tr
    @summary: transpose of a matrix
    @param a: input matrix
    @param ans: result
    @param r: row size  (first matrix)
    @param c: column size (first matrix)
    @return : none
*/
void _tr(float *a, float *ans, uint8_t r, uint8_t c){
    uint8_t i = 0;
    uint8_t j = 0;
    for(i = 0; i < r; i++){
        for(j = 0; j < c; j++){ 
            ans[j*r+i] = a[i*c+j];
        }
    }
}

/**
    @function: _inv
    @summary: LUP decomposition of a matrix
    @param a: input matrix
    @param ia: inverse of a matrix
    @param size: the matrix size
    @return : uint8_t
    
    @note: should be square matrix
    @source: https://en.wikipedia.org/wiki/LU_decomposition
*/
uint8_t _inv(float *a, float *ia, int size) {
    int p[size];
    float tmp[size][size];
    _copy(a, *tmp, size, size);    
    // LUP factorisation
    if(!_lup(*tmp, p, size)) return 0; // matrix singular
    
    for (uint8_t j = 0; j < size; j++) {
        for (uint8_t i = 0; i < size; i++) {
            if(p[i] == j) 
                ia[i*size+j] = 1.0;
            else
                ia[i*size+j] = 0.0;

            for (uint8_t k = 0; k < i; k++)
                // ia[i*size+j] -= tmp[i*size+k] * ia[k*size+j];
                ia[i*size+j] -= tmp[i][k] * ia[k*size+j];
        }
        for(int i = (size - 1); i >= 0; i--){
            for(uint8_t k = i + 1; k < size; k++){
                ia[i*size+j] -= tmp[i][k] * ia[k*size+j];
            }
            ia[i*size+j] = ia[i*size+j] / tmp[i][i];
        }
    }
    return 1;
}

/**
    @function: _set_m
    @summary: return a sub matrix given a matrix by deleting a specific row and
              column
    @param a: input matrix
    @param ans: the sub matrix
    @param size: matrix size
    @return : float
    
    @note: only square matrix for now
*/
void _sub_m(float *a, float *ans, uint8_t r, uint8_t c, uint8_t size){
    uint8_t i = 0;
    uint8_t j = 0;
    uint8_t k = 0;
    uint8_t l = 0;
    for(i = 0; i < size; i++){
        if(i == r) continue;
        l = 0;
        for(j = 0; j < size; j++){
            if(j == c) continue;
            ans[k*(size-1)+l] = a[i*size+j];
            l++;
        }
        k++;
    }
}

/**
    @function: _det
    @summary: determinant of a matrix
    @param a: input matrix
    @param size: matrix size
    @return : float
*/
float _det(float *a, uint8_t size){
    uint8_t i = 0;
    int sign = 1;
    float sub_mat[size-1][size-1];
    float det = 0;
    if(size == 1) return *a;
    for(i = 0; i < size; i++){
        _sub_m(a, *sub_mat, 0, i, size);
        det += sign * a[i] * _det(*sub_mat, size-1);
        sign = -sign;
    }
    return det;
}

/**
    @function: _inv2
    @summary: matrix inverse based on cofactor methods
    @param a: input matrix
    @param ans: inverse
    @param size: matrix size
    @return : uint8_t
*/
uint8_t _inv2(float *a, float *ans, uint8_t size){
    // md: matrix determinant
    float md = _det(a, size);
    if(md == 0.0) return 0; // singular matrix
    uint8_t i = 0;
    uint8_t j = 0;
    int8_t sign = 1;
    float sub_mat[size-1][size-1];
    float tmp[size][size];
    for(i = 0; i < size; i++){
        for(j = 0; j < size; j++){
            _sub_m(a, *sub_mat, i, j, size);
            ans[i*size+j] = sign * _det(*sub_mat, size-1);
            sign *= -1;
        }
        if((size % 2) == 0) sign *= -1;
    }
    _copy(ans, *tmp, size, size);
    _tr(*tmp, ans, size, size);
    _scale(ans, (1/md), ans, size, size); 
    return 1;
}
#endif
