""" Quadcopter simulator
    
    @author: AMOUSSOU Z. Kenneth
    @date: 10-02-2019
    @version:
    @dependencies:
        numpy:
            [version]: 1.12.1
        matplotlib:
            [version]: 2.0.2
"""
import sys
import numpy as np
import matplotlib.pyplot as plt

class Quadcopter:
    """ Generic simulator of a quadcopter
    """
    
    def __init__(self, config: dict, init_state: list, sampling_time: int):
        """ Constructor
            :param config:
                ["weight"]  : quad's weight
                ["ix"]      : inertia momentum around x-axis
                ["iy"]      : inertia momentum around y-axis
                ["iz"]      : inertia momentum around z-axis
                ["i"]       : inertia momentum
                ["l"]       : lenght from quad to motor center
                ["d"]       : propeller's drag coefficient
                ["t"]       : propeller's thrust coefficient
            :param init_state: initial state of the quad as a column vector
            :param sampling_time: sampling time for discret modeling
        """
        self._st = sampling_time                # sampling time
        self._A = np.mat(np.zeros((12, 12)))    # state matrix
        self._B = np.mat(np.zeros((12, 4)))     # control matrix
        self._W = np.mat(np.zeros((12,))).T     # 
        try:
            self._w = config["weight"]
            self._i = config["i"]
            self._ix = config["ix"]
            self._iy = config["iy"]
            self._iz = config["iz"]
            self._d = config["d"]
            self._t = config["t"]
            self._l = config["l"]
        except:
            print("An error occur during configuration")
            sys.exit(0)
        # initial action - No action performed at the beginning
        init_action = np.mat(np.zeros((4,))).T
        self.update(init_state, init_action)
    
    def model(self, state: list, action: list) -> list:
        """ core of the simulator
            :param state: current state of the quad as a column vector
            :param action: current action taken as a column vector
            :return: the next state of the quad given the current state and 
            action
            
            X(t+1) = [A]X(t) + [B]u + [W]
        """
        return ((self._A * state) + (self._B * action) + self._W)
    
    def update(self, state: list, action: list) -> None:
        """ Update the state and control matrices given the current state
            :param state: quad's state to compute new state matrices
            :param action: action to be performed
        """
        omega = np.sqrt(self.actionToSpeed(action))
        omega[0] = -omega[0]
        omega[2] = -omega[2]
        omega = omega.sum()
        # Update matrix A
        for i in range(6):
            self._A[2*i:2*i+2,2*i:2*i+2] = np.mat([[1, self._st], [0, 1]])
        self._A[7, 9] = self._st*(((self._iy - self._iz)*state[11]/self._ix)-(
                         self._i*omega/self._ix))
        self._A[9, 7] = self._st*(((self._iz - self._ix)*state[11]/self._iy)-(
                         self._i*omega/self._iy))
        self._A[11, 7] = self._st*(self._ix - self._iy)*state[9]/self._iz
        # Update matrix B
        self._B[1, 0] = (1/self._w)*(np.sin(state[10])*np.sin(state[6]) + 
                        np.cos(state[10])*np.sin(state[8])*np.cos(state[6]))
        self._B[3, 0] = (1/self._w)*(-np.cos(state[10])*np.sin(state[6]) + 
                        np.sin(state[10])*np.sin(state[8])*np.cos(state[6]))
        self._B[5, 0] = (1/self._w)*(np.cos(state[8])*np.cos(state[6]))
        self._B[7, 1] = 1/self._ix
        self._B[9, 2] = 1/self._iy
        self._B[11, 3] = 1/self._iz
        self._B = self._st*self._B
        # Update matrix W
        self._W[5] = -9.81*self._st
        return None
    
    def simulate(self, state: list, action: list, time:int) -> list:
        """ Run the model for a certain time and log it's state over time
            :param state: initial state to start the similation
            :param action: action to perform during the simulation
            :param time: duration of the simulation in number of time step
        """
        logs = []
        current_state = state
        for i in range(time):
            self.update(current_state, action)
            next_state = self.model(current_state, action)
            logs.append(next_state)
            current_state = next_state
        return logs        
    
    def actionToSpeed(self, action: list) -> list:
        """ Compute the speed of each motor of the quad given it's current set
            of action
            :param action: current action
        """
        omega2 = np.zeros((4,))
        omega2[0] = ((action[0]/(4*self._t)) - (action[2]/(2*self._t*self._l)) - 
                    (action[3]/(4*self._d)))
        omega2[1] = ((action[0]/(4*self._t)) - (action[1]/(2*self._t*self._l)) +
                    (action[3]/(4*self._d)))
        omega2[2] = ((action[0]/(4*self._t)) + (action[2]/(2*self._t*self._l)) -
                    (action[3]/(4*self._d)))
        omega2[3] = ((action[0]/(4*self._t))+(action[1]/(2*self._t*self._l)) +
                    (action[3]/(4*self._d)))
        return omega2

if __name__ == "__main__":
    config = dict()
    # Configuration of quad for simulation
    config["weight"] = 0.9 # Kg
    config["ix"] = 8.1e-03
    config["iy"] = 8.1e-03
    config["iz"] = 14.2e-03
    config["i"] = 104e-06
    config["l"] = 0.05
    config["d"] = 1.1e-06
    config["t"] = 54.2e-06
    
    sampling_time = 0.005 # 5ms
    init_state = np.mat(np.zeros((12,))).T
    
    quad = Quadcopter(config, init_state, sampling_time)
    logs = quad.simulate(init_state, np.mat([40, 0, 0, 0]).T, 30)
    x = []
    y = []
    z = []
    phi = []
    theta = []
    psi = []
    # retrieve useful data
    for log in logs:
        x.append(log[0].sum())
        y.append(log[2].sum())
        z.append(log[4].sum())
        phi.append(log[6].sum())
        theta.append(log[8].sum())
        psi.append(log[10].sum())
    # Plot graphs
    grid = plt.GridSpec(2, 3, wspace=0.5, hspace=0.5)
    figure = plt.figure(1)
    ax = figure.add_subplot(grid[0, 0])
    ax.set_title("X")
    ay = figure.add_subplot(grid[0, 1])
    ay.set_title("Y")
    az = figure.add_subplot(grid[0, 2])
    az.set_title("Z")
    phix = figure.add_subplot(grid[1, 0])
    phix.set_title("Phi")
    thetax = figure.add_subplot(grid[1, 1])
    thetax.set_title("Theta")
    psix = figure.add_subplot(grid[1, 2])
    psix.set_title("Psi")
    
    t = [j for j in range(30)]
    ax.plot(t, x)
    ay.plot(t, y)
    az.plot(t, z)
    phix.plot(t, phi)
    thetax.plot(t, theta)
    psix.plot(t, psi)
    plt.show()
