/**
    @author: AMOUSSOU Z. Kenneth
    @date: 30-01-2019
    @version:
**/
#include <Servo.h>
#define OUT1 PA2
#define OUT2 PA3
#define OUT3 PA0
#define OUT4 PA1

Servo esp1;
Servo esp2;
Servo esp3;
Servo esp4;
String _buffer = "";

int cmd = 1000;

void setup(){
    esp1.attach(OUT1);
    esp2.attach(OUT2);
    esp3.attach(OUT3);
    esp4.attach(OUT4);
    delay(500);
    Serial1.begin(115200);
    delay(1000);
}

void loop(){
    if(Serial1.available()){
        _buffer = Serial1.readString();
        cmd = _buffer.toInt();
        if(cmd < 0) return;
        esp1.writeMicroseconds(cmd);
        Serial1.print('0');
        Serial1.flush();
    }
    delay(100);
}
