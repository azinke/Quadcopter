""" Performance evaluation  n°3
    For throttle command at 0.8
        - found the max load that the quad can handle
        - found the max pitch angle
    @author: AMOUSSOU Z. Kenneth
    @date: 14-02-2019
"""
from math import sqrt
from math import pi
from model.config import *
from model.propeller import Propeller
from model.motor import Motor
from model.esc import ESC
from model.battery import Battery
import numpy as np
# Build objects
prop = Propeller(prop_config, env)
motor = Motor(motor_config)
esc = ESC(esc_config)
bat = Battery(bat_config)

def solve(command: float) -> list:
    """ solve a system of non linear equation numéricaly
        :method: Newton Method
        :return: an array of solution
            - motor voltage Um
            - motor current Im
            - motor torque M
            - motor speed N
            [Um, Im, M, N]
    """
    error = 1e-12
    max_iteration = 200
    
    # initial solution
    # [ Um, Im, M, N ]
    solution = np.mat([12, 5.8, 0.02, 2000], dtype="float64").T
    
    # Jacobian matrix
    J = np.mat(np.zeros((4, 4)))
    J[0, 0] = 1/bat_config["voltage"]
    J[0, 1] = esc_config["output-resistor"]/bat_config["voltage"]
    J[1, 2] = 1
    J[1, 3] = -2 * (prop.getD()*(4*pi**2)/(60**2))
    J[2, 0] = 1
    J[2, 2] = -motor_config["no-load-voltage"] * motor_config["KV-constant"]
    J[2, 2] *= motor_config["armature-resistor"]
    J[2, 2] /= 9.55*(motor_config["no-load-voltage"] - 
                    (motor_config["no-load-current"]*
                    motor_config["armature-resistor"]))
    J[2, 3] = -(motor_config["no-load-voltage"] - (
                motor_config["no-load-current"]*
                motor_config["armature-resistor"]))
    J[2, 3] /= motor_config["no-load-voltage"] * motor_config["KV-constant"]
    J[3, 1] = 1
    J[3, 2] = J[2, 2]/motor_config["armature-resistor"]
    # Approximation based on solution
    f = np.mat([0, 0, 0, 0]).T
    f[0] = esc.getCommand(solution[0], solution[1], 
                          bat_config["voltage"])-command
    f[1] = solution[2] + J[1, 3]*(solution[3]**2)/2
    f[2] = (solution[0] + J[2, 2]*solution[2] + J[2, 3]*solution[3] - 
            motor_config["no-load-current"] * motor_config["armature-resistor"])
    f[3] = solution[1] + J[3, 2]*solution[2] - motor_config["no-load-current"]
    iteration = 0
    while (iteration <= max_iteration):
        delta = - J.I * f
        solution += delta
        # Update
        f[0] = esc.getCommand(solution[0], solution[1], 
                              bat_config["voltage"])-command
        f[1] = solution[2] + J[1, 3]*(solution[3]**2)/2
        f[2] = (solution[0] + J[2, 2]*solution[2] + J[2, 3]*solution[3] - 
           motor_config["no-load-current"] * motor_config["armature-resistor"])
        f[3] = solution[1] + J[3, 2]*solution[2]-motor_config["no-load-current"]
        # checking stop condition
        _err = sqrt((delta.T*delta).sum()) / sqrt((solution.T*solution).sum())
        if (_err < error) and (sqrt((f.T*f).sum()) < error):
            # print(solution)
            return solution
            break
        iteration = iteration + 1
    if iteration > max_iteration: print("Max iteration reach")

# Get Um, Im, M and N by solving non linear set of equation
[Um, Im, M, N] = solve(0.8)

# Matrix format cancelation
Um = Um.sum()
Im = Im.sum()
M = M.sum()
N = N.sum()

print("Motor voltage Um: ", Um, " V")
print("Motor current Im: ", Im, " A")
print("Motor torque M: ", M, " Nm")
print("Motor speed N: ", N, " rpm")

# Thrust
T = prop.getB()*((N*2*pi/60)**2)
print("Thrust: ", T, " N")
# Max load
G_max = (((quad_config["nb-propulsor"]*T*1000)/env["gravity"]) 
                    - quad_config["weight"])
print("Max load Gmax: ", G_max, " g")
# Max pitch angle
theta_max = np.arccos((quad_config["weight"]*env["gravity"]/1000)/
                    (quad_config["nb-propulsor"]*T))
print("Max pitch angle: ", theta_max, " rad - ", (180*theta_max/pi), "°")
