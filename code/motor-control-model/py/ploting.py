""" Plot some graph out of data
    @author: AMOUSSOU Z. Kenneth
    @date: 15-02-2019
"""
import matplotlib.pyplot as plt


if __name__ == "__main__":
    # data from experience
    raw_in = []
    raw_out = []
    linearized = []
    with open("../data.txt", "r") as fhandle:
        for line in fhandle:
            _buffer = line.split(", ")
            _buffer = [float(v.strip()) for v in _buffer]
            raw_in.append(_buffer[0])
            raw_out.append(_buffer[1])
            linearized.append(30967*_buffer[0]-9328.2)
    #Figure 1: output
    fig1 = plt.figure(1)
    plt.plot(raw_in, raw_out, label="Données expérimentales")
    plt.plot(raw_in, linearized, label="Modèle")
    plt.legend(framealpha=1, frameon=True)
    plt.title("Modèle du système de proplusion")
    plt.xlabel("Commande")
    plt.ylabel("Vitesse - RPM")
    
    # plot markers
    # marker1 = [0.4025 for x in range(len(raw_out))]
    # plt.plot(marker1, raw_out, label="Marqueur")
    
    # Cost function
    cost = []
    index = 0
    with open("cost_function.txt", "r") as fhandle:
        for line in fhandle:
            index += 1
            _buffer = line.strip()
            _buffer = _buffer.strip(";")
            # print(_buffer)
            cost.append(float(_buffer))
    fig2 = plt.figure(2)
    iterations = [i for i in range(index)]    
    plt.plot(iterations, cost, label="Fonction de coût")
    plt.legend()
    plt.xlabel("Nombre d'itération")
    plt.ylabel("Coût")
   
    # Show curves
    plt.show()
