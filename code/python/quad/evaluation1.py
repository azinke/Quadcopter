""" Performance evaluation  n°1
    compute the response of each part of the quad in hover condition
    @author: AMOUSSOU Z. Kenneth
    @date: 14-02-2019
"""
from math import sqrt
from math import pi
from model import config
from model.propeller import Propeller
from model.motor import Motor
from model.esc import ESC
from model.battery import Battery

# Build objects
prop = Propeller(config.prop_config, config.env)
motor = Motor(config.motor_config)
esc = ESC(config.esc_config)
bat = Battery(config.bat_config)

# Thrust per motor
T = config.quad_config["weight"]*config.env["gravity"]
T = T/(1000 * config.quad_config["nb-propulsor"])
print("Thrust: ", T, " N")

# Speed of propeller
N = (30/pi) * sqrt(T/prop.getB())
print("Speed: ", N, " rpm")

# Torque
M = prop.getTorque(N)
print("Torque: ", M, " Nm")

# Motor voltage
Um = motor.getVoltage(N, M)
print("Motor voltage Um: ", Um, " V")
# Motor current
Im = motor.getCurrent(M)
print("Motor current Im: ", Im, " A")

# ESC command
cmd = esc.getCommand(Um, Im, config.bat_config["voltage"])
print("ESC command (%): ", cmd*100)
# ESC current
Ie = esc.getCurrent(Um, Im, config.bat_config["voltage"])
print("ESC current Ie: ", Ie, " A")

# Battery current
Ib = bat.getCurrent(config.quad_config["nb-propulsor"], Ie, 
                    config.quad_config["iother"])
print("Battery current Ib: ", Ib, " A")
# ESC voltage
Ue = esc.getVoltage(config.bat_config["voltage"], Ib, 
                    config.bat_config["internal-resistor"])
print("ESC voltage Ue: ", Ue, " V")

# Time of endurance of the quad
Thover = bat.getTimeOfEndurance(config.quad_config["nb-propulsor"], Ie, 
                                config.quad_config["iother"])
print("Time of endurance: ", Thover, " min")
