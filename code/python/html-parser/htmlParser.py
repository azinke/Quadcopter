""" HTML Parser
    Parse html file and convert it in header file for esp32
    @author: AMOUSSOU Z. Kenneth
"""

def parse(html: str, header: str) -> bool:
    """ convert an html file into header (.h) file
        :param html: html file
        :param header: header file
        :return: true if success
    """
    content = "const String html = "
    with open(html, "r") as fhtml:
        for line in fhtml:
            if(line.strip() == ""): continue
            # if "<!--" in line: continue
            if line.strip().startswith("//"): continue
            if "--" in line: continue
            content += "\"" + line.strip().replace("\"", "'")  + "\"\\"
            content += "\n"
    content += ";"

    with open(header + ".h", "w") as fheader:
        fheader.write(content)
    return True

if __name__ == "__main__":
    if parse("./index.html", "index"): print("Success!")
    else: print("Failed!")
