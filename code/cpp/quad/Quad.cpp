/** Quadcopter
 * 
 * @author: AMOUSSOU Z. Kenneth
 * @date: 22-02-2019
 * @version: 
 */

#include "Quad.h"
#include "mat.h"    // self made matrix computation library
// #include <bmp180.h> // pressure sensor
// #include <BNO055.h> // inertial measurement unit

//
// State variables
//
#ifdef IMU
BNO055 imu;
EUL angles; // Euler's angles
XYZ gyro;   // angular velocities
#endif
#ifdef BMP
BMP180 bmp;
#endif

//PID altitudeControl(0.5, 0.4, 0.5);

/**
 * @func: constructor
 * @param: none
 * @return: null
 */
Quad::Quad(){
    // null
}

/**
 *  @func: begin
 *  @summary: initialize the quad's parameters
 *  @param: 
 *  @return:
 */
void Quad::begin(){
    // init quad state
    _null(quad_state, 1, 6);
    
    // sampling time
    st = 0.004;        // s
    alt_st = 0.1;      // s

    l = 0.105;         // m
    
    // moment of inertia without the battery
    // so wrong to use these ones
    ix = 1.079e-03;    // kg.m²
    iy = 1.079e-03;    // kg.m²
    iz = 2.007e-03;    // kg.m²
    
    // moment of inertia of the quad
    // ix = 9.344e-04;    // kg.m²
    // iy = 1.0129e-03;   // kg.m²
    // iz = 1.7663e-03;   // kg.m²
    
    // moment of inertia of propeller + motor
    i = 6.3604e-06;    // kg.m²
    
    b = 1.461e-06;     // thrust coefficient
    d = 1.752e-08;     // torque coefficient
    w = 0.444;         // kg
    horizon = 1000;      // 50 x st (s)
    
    // 
    // Init coef for control matrix
    //
    coef1 = 1/(4*b);
    coef2 = 1.4142/(4*b*l);
    coef3 = 1/(4*d);

    // init PID controller
    // minimum thrust: m*g = 0.444 * 9.81 = 4.35564
    // minimum vertical speed = gravity x sampling time = ~0.981 m/s
    //
    // maximum thrust: 10.89 (get from performance evaluation)
    // maximum vertical speed = (10.89/gravity) x sampling time= ~2.453
    //
    // The PID controller compute the correction for the error in speed
    // which should be converted back in thrust by multiplying the output
    // by [(1 / vertical sampling time) * quad's weight]
    //
    // So as the set point and the measurements are the distance the PID data
    // should be scaled in "speed". vertical speed = distance/sampling time
//    altitudeControl.setLimits(0.981, 2.453);
//    altitudeControl.setSamplingTime(alt_st*1000);   // 100ms
//    altitudeControl.setPoint(2);            // 2 meters

    // init sensors
    #ifdef BMP
    bmp.begin(true);    // init internal complementary filter
    // TODO: init PID controller
    while(!bmp.isConnected()){
        #if QUAD_DEBUG == 0x01
        Serial.println("BMP missing on serial bus!");
        Serial.flush();
        #endif
    }    
    #endif
    #ifdef IMU
    imu.begin();
    imu.setUnits(BNO_ACC_MS2, BNO_GYR_RAD, BNO_TEMP_DC, BNO_EUL_RAD);
    while(!imu.isConnected()){
        #if QUAD_DEBUG == 0x01
        Serial.println("BNO missing on serial bus!");
        Serial.flush();
        #endif
    }
    // update the quad state from IMU data
    updateState();
    #endif

    // 
    // Compute control matrix B
    //
    _null(*B, 4, 2);
    B[2][0] = st/ix;
    B[3][1] = st/iy;
    _tr(*B, *Bt, 4, 2);  // transpose of B
    
    // init control vector
    ux[0] = 0.5;// 4.356;
    ux[1] = 0.0;
    ux[2] = 0.0;
    ux[3] = 0.0;

    // init quad's motors
    updateSpeed();

    //
    // Init state matrix
    //
    _null(*A, 4, 4);
    _eye(*A, 4);
    A[0][2] = st;
    A[1][3] = st;
    // debug(*A, 6, 6, "A(init): ");

    //
    // Init Q and R matrices
    //
    _eye(*Q, 4);
    _eye(*R, 2);
    /*Q[0][0] = 1;
    Q[1][1] = 1;
    Q[2][2] = 1;
    Q[3][3] = 100; */
    //
    // Init Pt
    //
    // _copy(*Q,*Pt, 4, 4);
    reset();
}

/**
 * @func: getOmega
 * @param: none
 * @return:
 */
float Quad::getOmega(){
    return (-speed[0]+speed[1]-speed[2]+speed[3]);
}

/**
 * @func: updateSpeed
 * @param: none
 * @return:
 */
void Quad::updateSpeed(){
    // TODO: limit the max values of ux command
    p1 = ux[0]*coef1;
    p2 = ux[1]*coef2;
    p3 = ux[2]*coef2;
    p4 = ux[3]*coef3;
    speed[0] = sqrt(p1 + p2 + p3 + p4);
    speed[1] = sqrt(p1 - p2 + p3 - p4);
    speed[2] = sqrt(p1 - p2 - p3 + p4);
    speed[3] = sqrt(p1 + p2 - p3 - p4);
}

/**
 * @func: update
 * @summary: core of the attitude controller
 * @param sc: vector[6] (set trajectory)
 * @return:
 *
 */
void Quad::update(float *sc){ 
    #if QUAD_PROFILING == 1
        unsigned long t = micros();
    #endif
    uint16_t h = 0;
    // update the quad state vector
    updateState();
    
//    reset();
    for(h = 0; h < horizon; h++){
        // Compute Lt
        _mul(*Bt, *Pt, *ans1, 2, 4, 4);
        _mul(*ans1, *B, *ans0, 2, 4, 2);
        _add(*R, *ans0, *ans3, 2, 2);
        if(!_inv(*ans3, *ans0, 2)){
            #if QUAD_DEBUG == 1
            Serial.println("singular matrix - Lt");   
            #endif
        }
        _mul(*ans0, *Bt, *ans1, 2, 2, 4); // result in: ans1
        _mul(*ans1, *Pt, *ans2, 2, 4, 4);       // result in: ans2
        _mul(*ans2, *A, *ans1, 2, 4, 4);      // result in: ans1
        _scale(*ans1, -1, *Lt, 2, 4);         // Lt
        debug(*Lt, 2, 4, "Lt: ");
        
        // Pt
        _tr(*A, *ans4, 4, 4);      // result in: ans4 - A transpose
        _mul(*ans4, *Pt, *ans5, 4, 4, 4);
        _mul(*ans5, *A, *ans4, 4, 4, 4);
        _mul(*ans5, *B, *ans6, 4, 4, 2);
        _mul(*ans6, *Lt, *ans5, 4, 2, 4);
        _add(*Q, *ans4, *Pt, 4, 4);
        _add(*Pt, *ans5, *Pt, 4, 4);
        // debug(*Pt, 4, 4, "Pt: ");

        // Compute Ot
        _mul(*R, *Bt, *ans1, 2, 2, 4);
        _scale(*ans1, -1, *Ot, 2, 4);
        debug(*Ot, 2, 4, "Ot: ");

        // Compute Es
        // Should be updated afterward with a set trajectory
        if(!_inv(*R, *ans3, 2)){
            #if QUAD_DEBUG == 1
            Serial.println("singular matrix - Es");   
            #endif
        }
        _mul(*B, *ans3, *ans6, 4, 2, 2);
        _mul(*ans6, *Bt, *ans4, 4, 2, 4);
        _mul(*ans4, *Pt, *ans5, 4, 4, 4);
        _sub(*A, *ans5, *ans4, 4, 4);
        _tr(*ans4, *ans5, 4, 4);
        _mulv(*ans5, Es, tmpv1, 4, 4);
        _mulv(*Q, sc, tmpv2, 4, 4);
        // debug(tmpv2, 1, 6,"tmpv2: ");
        _sub(tmpv1, tmpv2, Es, 1, 4);
        // debug(Es, 1, 4,"Es: ");
       
        // compute new state from the quad model
        _mulv(*A, quad_state, tmpv1, 4, 4);
        _mulv(*B, ux+1, tmpv2, 4, 2);
        _add(tmpv1, tmpv2, quad_state, 1, 4);
        debug(quad_state, 1, 4, "State: ");

        // Compute next action
        _mulv(*Lt, quad_state, tmpv3, 2, 4);
        _mulv(*Ot, Es, tmpv4, 2, 4);

        _add(tmpv3, tmpv4, ux+1, 1, 2);
    } // end for
    // update ux[0] from altitude controller
    // ux[0] = (w/alt_st) * altitudeControl.getOutput((int)quad_altitude);
    #if QUAD_DEBUG == 0x01
    Serial.println("End of Horizon!");
    Serial.println();
    Serial.println();
    #endif
    
    // clamping moment values
    if(ux[1] > MAX_MOMENT) ux[1] = MAX_MOMENT;
    if(ux[1] < -MAX_MOMENT) ux[1] = -MAX_MOMENT;

    if(ux[2] > MAX_MOMENT) ux[2] = MAX_MOMENT;
    if(ux[2] < -MAX_MOMENT) ux[2] = -MAX_MOMENT;

    if(ux[3] > MAX_MOMENT) ux[3] = MAX_MOMENT;
    if(ux[3] < -MAX_MOMENT) ux[3] = -MAX_MOMENT;
    
    ux[3] = 0.;
    
    // !!!! VERY IMPORTANT !!!!
    // TODO: Do not update the state matrix
    // But keep the line below for tracking purpose (for now) - Delete it later
    // on
    // updateStateMatrix(); // update state matrix
    
    // debug(*A, 4, 4, "A(init): ");
    // debug(ux, 1, 4, "Control vector: ");
    #if QUAD_PROFILING == 1
    t = micros() - t;
    Serial.print("Timing: ");
    Serial.print(t);
    Serial.println(" µs");
    #endif
}

/**
 * @func: getCommand
 * @summary: compute the right command to update the motor's speed
 * @param sc: vector[6] (set trajectory)
 * @return:
 *
 */
void Quad::getCommand(float *cmd){
    uint8_t i = 0;
    updateSpeed();       // update the speed
    for(i = 0; i < 4; i++){
        // convert speed in rpm before computing the appropriate command
        cmd[i] = ((speed[i]*9.5493)+9328.2)/30967;
    }
}

/**
 * @func: debug
 * @param m: pointer on a matrix or a vector
 * @param r: number of row
 * @param c: number of column
 * @param text: string to display that describe the data to print out
 * @return: none
 */
void Quad::debug(float *m, uint8_t r, uint8_t c, String text=""){
    #if QUAD_DEBUG == 1
    Serial.println(text);
    uint8_t i = 0;
    uint8_t j = 0;
    for(i = 0; i < r; i++){
        for(j = 0; j < c; j++){
            Serial.print(m[i*c+j], 4);
            Serial.print(" ");
            Serial.flush();
        }
        Serial.println();
        Serial.flush();
    }
    Serial.println();
    Serial.flush();
    #endif
}

/**
 * @func: updateState
 * @summary: update the state vector of the attitude controller
 *           the quad's state is build out of the IMU data
 * @param: none
 * @return none:
 */
void Quad::updateState(){
    #ifdef IMU
    angles = imu.getAngles();
    gyro = imu.getGyro();
    quad_state[0] = angles.roll;
    quad_state[1] = angles.pitch;
    quad_state[2] = gyro.x;
    quad_state[3] = gyro.y;
    #endif
    #ifdef BMP
    // quad_altitude = bmp.getRelativeAltitude(); 
    #endif
    
    #if QUAD_DEBUG == 1
    debug(quad_state, 1, 4, "Quad state: ");
    debug(&quad_altitude, 1, 1, "Quad altitude: ");
    #endif
}

/**
 * @func: readAttitude
 * @summary: read the quadcopter's attitude
 * @param:
 *      state: vector containing the attitude
 * @return none:
 */
void Quad::readAttitude(float *state){
    #ifdef IMU
    angles = imu.getAngles();
    gyro = imu.getGyro();
    state[0] = angles.roll;
    state[1] = angles.pitch;
    state[2] = gyro.x;
    state[3] = gyro.y;
    #endif
}

/**
 * @func: reset
 * @summary: re-initialize Pt, Es for the next horizon
 * @param: none
 * @return none:
 */
void Quad::reset(){
    _copy(*Q,*Pt, 4, 4);    // Pt = Q
    _null(Es, 1, 4);        // Es = 0
}

/**
 * @func: printAngles
 * @summary: print the quad euler angles
 * @param: none
 * @return none:
 */
void Quad::printAngles(){
    angles = imu.getAngles();
    Serial.print(angles.roll);
    Serial.print(",");
    Serial.print(angles.pitch);
    Serial.print(",");
    Serial.println(angles.yaw);
}

/**
 * @func: setUx0
 * @summary: test method to set ux[0] directly
 * @param value: new value for ux[0]
 * @return: none
 */
void Quad::setUx0(float value){
    ux[0] = value;
}

float Quad::getUx0(){
    return ux[0];
}
