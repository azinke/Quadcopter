""" Numeric filter coefficient
    plot the curve of numeric filter coefficient
    @author: AMOUSSOU Z. Kenneth
    @date: 15-02-2019
"""
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d

def computeA(freq: list, time: list) -> list:
    """ compute the coef "a"
        :param freq: frequency range
        :param time: sampling time
        :return: list of coefficient
    """
    return 1/(1+2*3.1415*freq*time)

if __name__ == "__main__":
    frequency = [i for i in range(100)]
    sampling_time = []
    i = 0.001
    while i <= 0.05:
        sampling_time.append(i)
        i += 0.001
    F, T = np.meshgrid(frequency, sampling_time)
    A = computeA(F, T)
    fig1 = plt.figure(1)
    ax = plt.axes(projection="3d")
    # ax.plot_surface(F, T, A, rstride=1, cstride=1, cmap="viridis", 
    #                edgecolor="none")
    ax.plot_wireframe(F, T, A, color='black')
    # ax.contour3D(F, T, A, 100, cmap='binary')
    ax.set_title("Coefficient a")
    ax.set_xlabel("Fréquence - Hz")
    ax.set_ylabel(" Temps d'échantillonnage - s")
    ax.set_zlabel("a")
    
    fig2 = plt.figure(2)
    a = []
    # samping time = 5ms
    for f in frequency:
        a.append(computeA(f, 0.005))
    plt.plot(frequency, a, label="Coefficient a", color="orange")
    plt.xlabel("Fréquence - Hz")
    plt.ylabel("a")
    plt.legend()
    plt.show()
