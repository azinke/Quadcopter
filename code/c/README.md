# Matrix calculs

This header file provide basics matrix calculs functions for linear algebra. 
The aim of it's development is to have a fast matrix calculus library for 
microcontrollers. That's the reason why it's developped in `c` language.

## Note

The order of computation of matrix multiplication for a set of matrices should 
be considered by the programmer.

## Doto

- add more functions
- optimise the code for inverse matrix computations
