""" Performance evaluation  n°4
    Found the quad's max speed and max flight distance by solving an
    optimisation (maximization) problem
    @author: AMOUSSOU Z. Kenneth
    @date: 14-02-2019
"""
from math import sqrt
from math import pi
from math import sin
from math import cos
from math import tan
from model.config import *
from model.propeller import Propeller
from model.motor import Motor
from model.esc import ESC
from model.battery import Battery
import numpy as np
# Build objects
prop = Propeller(prop_config, env)
motor = Motor(motor_config)
esc = ESC(esc_config)
bat = Battery(bat_config)

# Drag coefficient
cd1 = 3
cd2 = 1.5
# cross sectional area of the quad
S = 0.0125  # m² ( 25cm x 5cm)

def computeLinearSpeed(theta: float) -> float:
    """ compute the linear speed of the quad based on the pitch angle
        :param theta: pitch angle in radian
        :return: actual speed in meter per second
    """
    _buf = cd2 * (1 - (cos(theta)**3))
    _buf += cd1 * (1 - (sin(theta)**3))
    _buf *= prop.getRho() * S
    _buf = (2*tan(theta)*quad_config["weight"]*env["gravity"]/1000)/_buf
    return sqrt(_buf)

def computeSpeed(theta: float) -> float:
    """ compute the speed of the quad given the pitch angle
        :param theta: pitch angle in rad
        :return: speed in rpm
    """
    _buf = (prop.getD()*(4*(pi**2)))*quad_config["nb-propulsor"]*cos(theta)
    _buf = (quad_config["weight"]*env["gravity"]/1000)/_buf
    return 60*sqrt(_buf)

def computeTorque(theta: float) -> float:
    """ compute the quad's motor torque given the pitch angle
        :param theta: pitch angle in rad
        :return: torque in Nm
    """
    _buf = (quad_config["weight"]*env["gravity"]/1000)
    _buf *= prop.getTorqueCoef()*prop_config["diameter"]
    _buf /= prop.getThrustCoef()*quad_config["nb-propulsor"]*cos(theta)
    return _buf

def computeFlightTime(theta: float) -> float:
    """ compute the flight time of the quad based on pitch angle
        :param theta: pitch angle in rad
        :return: endurance time in minute
    """
    speed = computeSpeed(theta)
    torque = computeTorque(theta)
    Um = motor.getVoltage(speed, torque)
    Im = motor.getCurrent(torque)
    Ie = esc.getCurrent(Um, Im, bat_config["voltage"])
    return bat.getTimeOfEndurance(quad_config["nb-propulsor"], Ie, 
                                  quad_config["iother"])

def computeDistance(theta: float) -> float:
    """ compute the distance travelled by the quad based on the pitch angle
        :param theta: pitch angle of the quad
        :return: distance in meter
    """
    return 60*computeLinearSpeed(theta)*computeFlightTime(theta)
    

if __name__ == "__main__":
    # max picth angle in radian
    theta_max = 1.36876583397
    angle = theta_max
    
    max_linear_speed_theta = 0
    max_linear_speed = 0
    
    max_distance_theta = 0
    max_distance = 0
    
    # Solving optimization problem greedyly
    while angle >= 0:
        linear_speed = computeLinearSpeed(angle)
        distance = computeFlightTime(angle)*linear_speed*60
        if linear_speed > max_linear_speed: 
            max_linear_speed = linear_speed
            max_linear_speed_theta = angle
        if distance > max_distance: 
            max_distance = distance
            max_distance_theta = angle
        angle -= 1e-3
    
    print("Max linear speed V({0}) = {1} m/s".format(max_linear_speed_theta,
        max_linear_speed))
    print("Max distance Z({0}) = {1} m".format(max_distance_theta,
        max_distance))
    print("Linear speed at Zmax V({0}) = {1} m/s".format(max_distance_theta,
        computeLinearSpeed(max_distance_theta)))
