""" Model a battery
    
    @author: AMOUSSOU Z. Kenneth
    @date: 13-02-2019
    @version:
"""
from sys import exit

class Battery:
    """ Battery modeling
        
        The goal of this model is to estimate the remaining flight time based 
        on the battery discharge level.
        :output:
            [1] time of endurance
            [2] battery output current
            [3] battery max output current
    """
    
    def __init__(self, config: dict):
        """ Constructor
            :param config: configuration data
        """
        try:
            self._cb = config["capacity"]
            self._cmin = 0.2 * self._cb
            self._rb = config["internal-resistor"]
            self._ub = config["voltage"]
            self._kb = config["c-rating"]
            self._weight = config["weight"]
        except:
            print("Missing configuration data for modeling the battery")
            exit(0)
    
    def getTimeOfEndurance(self, np: int, iesc: float, iother: float) -> float:
        """ compute the remaining time for the battery to left out
            :param np: the number of the propulsor available
            :param iesc: current needed by an esc to drive a given motor
            :param iother: current needed by other equipement
            :return: time of endurance in minute
        """
        return (self._cb - self._cmin)*0.06/self.getCurrent(np, iesc, iother)
    
    def getMaxCurrent(self) -> float:
        """ compute max current that can be sink from the battery
            :return: max battery output current
        """
        return (self._cb * self._kb)/1000
    
    def getCurrent(self, np: int, iesc: float, iother: float) -> float:
        """ compute the current delivered by the battery
            :param np: the number of the propulsor available
            :param iesc: current needed by an esc to drive a given motor
            :param iother: current needed by other equipement
            :return: output current of the battery
        """
        return (np * iesc + iother)

if __name__ == "__main__":
    # configuration of the battery
    config = {
        "capacity": 1800,               # milli ampere hour
        "internal-resistor": 0.01,      # ohm
        "voltage": 11.1,                # volt
        "c-rating": 30,                 # dimensionless
        "weight": 136                   # gram
    }
    
    bat = Battery(config)
    print("Max current: ", bat.getMaxCurrent(), "A")
