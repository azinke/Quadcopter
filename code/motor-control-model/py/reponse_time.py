""" Response Time
    Plot the curve of the speed in relation with the command
    @author: AMOUSSOU Z. Kenneth
    @date: 15-02-2019
"""
import matplotlib.pyplot as plt

if __name__ == "__main__":
    sampling_time = 0.5
    output = []
    time = []
    index = 0
    with open("../speed_step_unity.txt", "r") as fhandle:
        for line in fhandle:
            index += 1
            _buffer = line.split(",")
            _buffer = [float(x.strip()) for x in _buffer]
            time.append(index * sampling_time)
            output.append(_buffer[1]*60)
    plt.plot(time, output, label="Vitesse", color="grey")
    plt.xlabel("Temps - seconde")
    plt.ylabel("Vitesse - RPM")
    plt.show()
            
