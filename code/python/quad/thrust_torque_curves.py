""" Plot thrust and torque curves
    Plot curves from the quadcopter model
    @author: AMOUSSOU Z. Kenneth
    @date: 14-02-2019
"""
from model.propeller import Propeller
from model.config import prop_config as config
from model.config import env
import matplotlib.pyplot as plt

if __name__ == "__main__":
    prop = Propeller(config, env)
    speed = [500*i for i in range(40)]
    thrust = []
    torque = []
    for data in speed:
        thrust.append(prop.getThrust(data))
        torque.append(prop.getTorque(data))
    plt.figure(1)
    plt.plot(speed, thrust, "-g")
    plt.xlabel("Vitesse - rpm")
    plt.ylabel("Poussée - N")
    plt.figure(2)
    plt.plot(speed, torque, "-g")
    plt.xlabel("Vitesse - rpm")
    plt.ylabel("Couple - Nm")
    plt.show()
