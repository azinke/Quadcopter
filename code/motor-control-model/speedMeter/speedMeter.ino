/**
    @summary: Measure the speed of a DC motor based on infrared LED
    @author: AMOUSSOU Z. Kenneth
    @date: 22-01-2019
    @version: 1.0
*/
#include <Servo.h>
#include <SoftwareSerial.h>
#define PIN 2

SoftwareSerial driver(10, 11);// RX: 10 | TX: 11

volatile uint32_t counter = 0;
uint16_t cmd = 1000;
bool isTestDone = false;

unsigned long now = 0;

void setup(){
    pinMode(PIN, INPUT);
    delay(50);
    Serial.begin(500000);
    delay(100);
    driver.begin(115200);
    delay(100);
    attachInterrupt(digitalPinToInterrupt(PIN), ping, FALLING);
    delay(10000);
    Serial.println("Begining test beching...");
    cmd = 1400;
    now = millis();     // initialize current time
}

void loop(){
    if(isTestDone) return;
    
    // update command
    if((millis() - now) >= 10000){
        cmd += 100;          // increment command
        driver.println(cmd); // write new command on output
        while(!driver.available());
        driver.read();
        //delay(750);
        counter = 0;    // Init counter
        now = millis(); // update current time for next command update
    }
    // disable interrupt
    delay(100); // wait 0.1s  
    noInterrupts();
    // process counter to compute speed in tr/s
    // Serial.println(String(cmd)+ ", " + String(counter));
    Serial.println(String(cmd)+ ", " + String(counter) + ", " + String(millis()));
    Serial.flush();
    //counter = 0;
    // enable interrupt
    interrupts();
    if(cmd > 1500){ 
        Serial.println("End!");
        isTestDone = true; 
        driver.println(1000);
        delay(1000);
        noInterrupts(); // disable interrupts
    }
}

void ping(){
    counter++;    
}
