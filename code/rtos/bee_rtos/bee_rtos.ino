/* Bee Real Time Operating System
   @author: AMOUSSOU Z. Kenneth
   @date: 20-03-2019
   @version: 1.0
   @processor: STM32F103C8B
*/
// RTOS
#include <MapleFreeRTOS900.h>

// BNO055 library used to computed Euler angles
#include <BNO055.h>

// Barometric sensor library
#include <bmp180.h>

// PID controller
#include <PID.h>

// Servo library used to handle ESC in order to control motors speed
#include <Servo.h>

/* @def: motor
*/
#define M1  PA0
#define M2  PA1
#define M3  PA2
#define M4  PA3

/* @def: wireless receiver
*/
#define RX1 PA6
#define RX2 PA7

#define APP_DEBUG   0x00

/* def PROFILING: enable or disable code profiling
 */
#define PROFILING   0x00
float imu_profil = 0.;
float control_profil = 0.;
float receiver_profil = 0.;
float buf1 = 0;
float buf2 = 0;
float buf3 = 0;

/* @var bmp: pressure sensor
 */
BMP180 bmp;

/* @var hController: height PID controller
   :param kp: 0.5
   :param ki: 0.01
   :param kd: 0
*/
PID hController(4, 0.001, 0);

/* @var imu: inertial measurement unit, used to compute Euler angles
   @var data: struct that hold data get from imu
*/
BNO055 imu;
EUL anglesData;
XYZ gyroData;

/*
   Quad parameters
*/
// sampling time
/* @var st: imu sampling time
*/
float st = 0.004;        // s
float alt_st = 0.1;      // s

float l = 0.105;         // m

// moment of inertia without the battery
// so wrong to use these ones
float ix = 1.079e-03;    // kg.m²
float iy = 1.079e-03;    // kg.m²
float iz = 2.007e-03;    // kg.m²

// moment of inertia of the quad
// float ix = 9.344e-04;    // kg.m²
// float iy = 1.0129e-03;   // kg.m²
// float iz = 1.7663e-03;   // kg.m²

// moment of inertia of propeller + motor
float i = 6.3604e-06;    // kg.m²

float b = 1.461e-06;     // thrust coefficient
float d = 1.752e-08;     // torque coefficient
float w = 0.444;         // kg

float coef1 = 1 / (4 * b);
float coef2 = 1.4142 / (4 * b * l);
float coef3 = 1 / (4 * d);

float p1 = 0.;
float p2 = 0.;
float p3 = 0.;
float p4 = 0.;

/* @var: electronic speed control
   esc1 : control motor 1
   esc2 : control motor 2
   esc3 : control motor 3
   esc4 : control motor 4
*/
Servo esc1;
Servo esc2;
Servo esc3;
Servo esc4;

/* @var rx1:
*/
volatile bool rx1_flag = false;
volatile uint32_t rx1_timer = 0;
volatile int32_t rx1_pulse_width = 0;

volatile bool rx2_flag = false;
volatile uint32_t rx2_timer = 0;
volatile int32_t rx2_pulse_width = 0;

/*
   @var on: prepare the quad for flight
   @var takeoff: schedule an autonomous take off of the quad
   @var land: schedule an autonomous landing of the quad
   @var up, down, forward, backward, right, left, yaw_right, yaw_left:
        direction variables
*/
bool on         = false;
bool takeoff    = false;
bool land       = false;
bool up         = false;
bool down       = false;
bool forward    = false;
bool backward   = false;
bool right      = false;
bool left       = false;
bool yaw_right  = false;
bool yaw_left   = false;

/* @var xReceiverSemaphore: create a semaphore to handle remote pilote's request
 * @var xHeightSemaphore: create a semaphore to handle height control
 */
SemaphoreHandle_t xReceiverSemaphore;
SemaphoreHandle_t xHeightSemaphore;

/* @var: state vector and set trajectory
*/
float height = 1.;
float state[4] = {0., 0., 0., 0.};
float trajectory[2] = {0., 0.};
float trajectoryError[2] = {0., 0.};

/* @var ux: command vector
*/
float ux[4] = { 1., 0., 0., 0. };

/* @var speed: vector of quad's motor speed
*/
float speed[4] = { 0., 0., 0., 0. };

/* @var cmd: vector that hold esc command
*/
float cmd[4] = { 0., 0., 0., 0. };

/* RL coef
    Lt[4] = { -0.00181314, -0.00581815, -0.00181314, -0.00581815 };
            { -0.04435618, -0.0117192, -0.04435618, -0.0117192 }
*/
const float Lt[4] = { -0.08507238, -0.0224767, -0.08507238, -0.0224767 };
// const float Ot[2] = {-3.7071,-3.7071 }; // error

/* @var test_timer: test time counter to stop the quad after 15 seconds
   @var is_stopped: stop the quad is 'true'
*/
uint32_t test_timer = 0;
bool is_stopped = false;
bool is_takeoff_done = false;
uint32_t takeoff_timer = 0;

/* @var is_request_sent: request the remote driver command
*/
bool is_request_sent = false;

//
// ######### Tasks #########
//

/* @task: vIMUReadTask
   @summary: read data out of imu and process Euler angles
   @priority: idle + 1
   @see: BNO055 imu
*/
static void vIMUReadTask(void *pvParameters) {
  /* @var xLastWakeTime: last wakeup time
     @var xFrequency: execution cycle of the task
  */
  TickType_t xLastWakeTime;
  const TickType_t xFrequency = pdMS_TO_TICKS(4);

  // init last time wake with current time on the first time call
  xLastWakeTime = xTaskGetTickCount();
  imu.begin();
  // configure BNO055
  imu.setUnits(BNO_ACC_MS2, BNO_GYR_RAD, BNO_TEMP_DC, BNO_EUL_RAD);
  imu.mapAxisSign(0, 0, 0);
  vTaskDelay(pdMS_TO_TICKS(200));
  anglesData = imu.getAngles();
  gyroData = imu.getGyro();
  state[0] = anglesData.pitch;
  state[1] = anglesData.roll;
  state[2] = gyroData.x;
  state[3] = gyroData.y;
  while (true) {
    #if PROFILING == 0x01
    buf1 = micros();
    #endif
    anglesData = imu.getAngles();
    gyroData = imu.getGyro();
    state[0] = 0.98 * state[0] + 0.02 * anglesData.pitch;
    state[1] = 0.98 * state[1] + 0.02 * anglesData.roll;
    state[2] = 0.98 * state[2] + 0.02 * gyroData.x;
    state[3] = 0.98 * state[3] + 0.02 * gyroData.y;
    #if PROFILING == 0x01
    imu_profil = micros() - buf1;
    #endif
    // schedule next execution of this task
    vTaskDelayUntil( &xLastWakeTime , xFrequency );
  }
  // delete Task (auto deletion)
  vTaskDelete(NULL);
}

/* @task: vControlTask
   @summary: compute the quad control strategy and apply
   @priority: idle + 1
   @see: state, speed,
*/
static void vControlTask(void *pvParamaters) {
  /* @var xLastWakeTime: last wakeup time
     @var xFrequency: execution cycle of the task
  */
  TickType_t xLastWakeTime;
  const TickType_t xFrequency = pdMS_TO_TICKS(4);

  // init last time wake with current time on the first time call
  xLastWakeTime = xTaskGetTickCount();
  while(true){
    #if PROFILING == 0x01
    buf2 = micros();
    #endif
    if(xReceiverSemaphore != NULL){
        // take semaphore
        if(xSemaphoreTake(xReceiverSemaphore, 1) == pdTRUE){
            if(on){ // on
                computeAttitude();
                writeCommand();
            }else{
                esc1.writeMicroseconds(950);
                esc2.writeMicroseconds(950);
                esc3.writeMicroseconds(950);
                esc4.writeMicroseconds(950);
            }
            // give semaphore back
            xSemaphoreGive(xReceiverSemaphore);
        }
    }
    #if PROFILING == 0x01
    control_profil = micros() - buf2;
    #endif
    // schedule next execution of this task
    vTaskDelayUntil( &xLastWakeTime , xFrequency );
  }
  // delete Task (auto deletion)
  vTaskDelete(NULL);
}

/* @task: vHeightControlTask
   @summary: compute the quad's height control strategy and apply
   @priority: idle + 2
   @see: height,
*/
static void vHeightControlTask(void *pvParameters){
    TickType_t xLastWakeTime;
    const TickType_t xFrequency = pdMS_TO_TICKS(100);

    // init last time wake with current time on the first time call
    xLastWakeTime = xTaskGetTickCount();

    // configure PID controller
    // param min: 1 Newton
    // param max: 10 Newton
    hController.setLimits(1, 10);
    hController.setSamplingTime(100);

    // init bmp sensor
    bmp.begin(true);
    while(true){
        hController.setPoint(height);
        if(height != 0){
            if(xHeightSemaphore != NULL){
                if(xSemaphoreTake(xHeightSemaphore, 10) == pdTRUE){
                    ux[0] = hController.getOutput(bmp.getRelativeAltitude());
                    xSemaphoreGive(xHeightSemaphore);
                }
            }
        }
        // schedule next execution of this task
        vTaskDelayUntil( &xLastWakeTime , xFrequency );
    }
    vTaskDelete(NULL);
}

/* @task: vReceiverTask
   @summary: read remote driver command
   @priority: idle + 3
   @see: receiverOne, reveiverTwo
*/
static void vReceiverTask(void *pvParamaters) {
  /* @var xLastWakeTime: last wakeup time
     @var xFrequency: execution cycle of the task
  */
  TickType_t xLastWakeTime;
  const TickType_t xFrequency = pdMS_TO_TICKS(100);

  // init last time wake with current time on the first time call
  xLastWakeTime = xTaskGetTickCount();

  // create semaphore
  vSemaphoreCreateBinary(xReceiverSemaphore);

  while (true) {
    #if PROFILING == 0x01
    buf3 = micros();
    #endif
    if(xReceiverSemaphore != NULL){
        // take the semaphore
        if(xSemaphoreTake(xReceiverSemaphore, 10) == pdTRUE){
            on = false;
            up = false;
            down = false;
            right = false;
            left = false;
            forward = false;
            backward = false;
            yaw_right = false;
            yaw_left = false;

            if (rx1_pulse_width < 1100) {
              on = false;
              // digitalWrite(PB11, LOW);
            }
            else {
              on = true;
              // digitalWrite(PB11, HIGH);
            }

            if (rx1_pulse_width > 1550) {
                // takeoff routine
                if ((rx2_pulse_width > 1100) && (rx2_pulse_width < 1200)){
                    takeoff = true;
                    // height = 1.;    // 1m
                    ux[0] = 3.5;
                }
                else if ((rx2_pulse_width > 1200) && (rx2_pulse_width < 1300)){
                    up = true;
                    // height += 0.1;
                    // limit max throttle to 8 for now
                    // if(height >= 4) height = 4;
                    ux[0] += 0.1;
                    if(ux[0] > 8) ux[0] = 8;
                }
                else if ((rx2_pulse_width > 1300) && (rx2_pulse_width < 1400)){
                    forward = true;
                }
                else if ((rx2_pulse_width > 1400) && (rx2_pulse_width < 1500)){
                    right = true;
                }
                else if ((rx2_pulse_width > 1500) && (rx2_pulse_width < 1600)){
                    yaw_right = true;
                }
            }

            if ((rx1_pulse_width > 1250) && (rx1_pulse_width < 1450)){
                if ((rx2_pulse_width > 1100) && (rx2_pulse_width < 1200)){
                    land = true;
                    // height = 0.;
                    ux[0] = 1.;
                }
                else if ((rx2_pulse_width > 1200) && (rx2_pulse_width < 1300)){
                    down = true;
                    // height -= 0.1;
                    // min throttle
                    // if(height < 1) height = 1;
                    ux[0] -= 0.1;
                    if(ux[0] < 1) ux[0] = 1;
                }
                else if ((rx2_pulse_width > 1300) && (rx2_pulse_width < 1400)){
                    backward = true;
                }
                else if ((rx2_pulse_width > 1400) && (rx2_pulse_width < 1500)){
                    left = true;
                }
                else if ((rx2_pulse_width > 1500) && (rx2_pulse_width < 1600)){
                    yaw_left = true;
                }
            }

            // give semaphore back
            xSemaphoreGive(xReceiverSemaphore);
        }
    }
    #if PROFILING == 0x01
    receiver_profil = micros() - buf3;
    #endif
    // schedule next execution of this task
    vTaskDelayUntil( &xLastWakeTime , xFrequency );
  }
  // delete Task (auto deletion)
  vTaskDelete(NULL);
}

/////////////////////////////////////////////////////////////////
//                       Test bench                            //
/////////////////////////////////////////////////////////////////
static void vDebugTask(void *pvParameters){
    Serial1.begin(115200);
    vTaskDelay(pdMS_TO_TICKS(50));
    TickType_t xLastWakeTime;
    const TickType_t xFrequency = pdMS_TO_TICKS(1000);

    // init last time wake with current time on the first time call
    xLastWakeTime = xTaskGetTickCount();
    while(1){    
        Serial1.print(state[0]);
        Serial1.print(",");
        Serial1.println(state[1]);

        Serial1.print(speed[0]);
        Serial1.print(",");
        Serial1.print(speed[1]);
        Serial1.print(",");
        Serial1.print(speed[2]);
        Serial1.print(",");
        Serial1.println(speed[3]);

        Serial1.println();

        Serial1.print("IMU: ");
        Serial1.println(imu_profil);
        Serial1.print("CONTROL: ");
        Serial1.println(control_profil);
        Serial1.print("RECEIVER: ");
        Serial1.println(receiver_profil);

        Serial1.println();

        Serial1.print("rx1: ");
        Serial1.println(rx1_pulse_width);
        Serial1.print("rx2: ");
        Serial1.println(rx2_pulse_width);

        Serial1.println();
        
        vTaskDelayUntil(&xLastWakeTime, xFrequency);
    }
    vTaskDelete(NULL);
}
/////////////////////////////////////////////////////////////////

/**
   @func: updateSpeed
   @param: none
   @return:
*/
inline void updateSpeed() {
  // TODO: limit the max values of ux command
  p1 = ux[0] * coef1;
  p2 = ux[1] * coef2;
  p3 = ux[2] * coef2;
  p4 = ux[3] * coef3;
  speed[0] = sqrt(p1 - p2 + p3 - p4);
  speed[1] = sqrt(p1 + p2 + p3 + p4);
  speed[2] = sqrt(p1 + p2 - p3 - p4);
  speed[3] = sqrt(p1 - p2 - p3 + p4);
}

/*
   @func: getCommand
   @summary: compute the right command to update the motor's speed
   @return:
*/
inline void getCommand() {
  uint8_t i = 0;
  updateSpeed();       // update the speed
  for (i = 0; i < 4; i++) {
    // convert speed in rpm before computing the appropriate command
    // cmd[i] = ((speed[i]*9.5493)+9328.2)/30967;
    cmd[i] = ((speed[i] * 9.5493) + 13515) / 32402;
  }
}

/* @func: writeCommand
   @param cmd: float point number vector computed form the controller
        value range: [0, 1]
        unit: dimensionless
        size: 4
   @return: none
*/
inline void writeCommand() {
  getCommand();
  esc1.writeMicroseconds(mapCommand(cmd[0]));
  esc2.writeMicroseconds(mapCommand(cmd[1]));
  esc3.writeMicroseconds(mapCommand(cmd[2]));
  esc4.writeMicroseconds(mapCommand(cmd[3]));
}

/* @func: mapCommand
   @desc: compute the matching pulse width given an command computed by
        the controller
   @param value:
        value range: [1000, 2000]
        unit: microseconds (µs)
   @espression:
        value * (maximum - minimum) + minimum
   @return: pulse with in microseconds
*/
uint16_t mapCommand(float value) {
  uint16_t pulse_width = ( (uint16_t)(2 * (value - 0.5) * 1000.) + 1000 );
  if (pulse_width > 2000) return 2000;
  else return pulse_width ;
}

/* @func: computeAttitude
   @desc: compute the optimal action based of the Riccati equation
   @see Lt: matrix get from training
   @see Ot: matrix get from training for trajectory following
   @return: none
*/
void computeAttitude() {
  // trajectoryError[0] = trajectory[0] - state[2];
  // trajectoryError[1] = trajectory[1] - state[3];
  ux[1] = Lt[0] * state[0] + Lt[1] * state[2]; // Ot[0] * trajectoryError[0];
  ux[2] = Lt[2] * state[1] + Lt[3] * state[3]; // Ot[1] * trajectoryError[1];
}

/* @func: initTimer3
   @desc: init the timer 3 for handling input capture on pin A6 annd A7
   @param: none
   @return: none
 */
void initTimer3(){
    Timer3.attachCompare1Interrupt(receiverOne); // PA6
    Timer3.attachCompare2Interrupt(receiverTwo); // PA7
    TIMER3_BASE->CR1 = TIMER_CR1_CEN;
    TIMER3_BASE->CR2 = 0;
    TIMER3_BASE->SMCR = 0;
    TIMER3_BASE->DIER = TIMER_DIER_CC1IE | TIMER_DIER_CC2IE;
    TIMER3_BASE->EGR = 0;
    TIMER3_BASE->CCMR1 = 0b100000001;
    TIMER3_BASE->CCMR2 = 0; // 0b100000001;
    TIMER3_BASE->CCER = TIMER_CCER_CC1E | TIMER_CCER_CC2E;
    
    TIMER3_BASE->CCER &= ~TIMER_CCER_CC1P; //Detect rising edge - channel 1.
    TIMER3_BASE->CCER &= ~TIMER_CCER_CC2P; //Detect rising edge - channel 2.
    TIMER3_BASE->PSC = 71;
    TIMER3_BASE->ARR = 0xFFFF;
    TIMER3_BASE->DCR = 0;
}

/* @func: setup
   @desc: initialization
   @param: none
   @return: none
*/
void setup() {
  // for testing
  // pinMode(PB11, OUTPUT);

  pinMode(RX1, INPUT);
  pinMode(RX2, INPUT);
  // attachInterrupt(RX1, receiverOne, CHANGE);
  // attachInterrupt(RX2, receiverTwo, CHANGE);

  // init timer 3
  initTimer3();

  /*
     Debugging
  */
  // Serial1.begin(115200);
  // delay(100);

  /*
     Init ESC
  */
  esc1.attach(M1);
  esc2.attach(M2);
  esc3.attach(M3);
  esc4.attach(M4);
  delay(500);
  esc1.writeMicroseconds(1000);
  esc2.writeMicroseconds(1000);
  esc3.writeMicroseconds(1000);
  esc4.writeMicroseconds(1000);
  delay(4000);

  // imu.begin();
  // imu calibration
  // calibrate();
  // digitalWrite(PB11, HIGH);
  delay(1000);

  //
  // create tasks
  //
  
  xTaskCreate(vIMUReadTask, "IMU",
              configMINIMAL_STACK_SIZE,
              NULL,
              tskIDLE_PRIORITY + 1,
              NULL);

  xTaskCreate(vControlTask, "CTRL",
              configMINIMAL_STACK_SIZE,
              NULL,
              tskIDLE_PRIORITY + 1,
              NULL);

  xTaskCreate(vReceiverTask, "RCV",
              configMINIMAL_STACK_SIZE,
              NULL,
              tskIDLE_PRIORITY + 3,
              NULL);
#if APP_DEBUG == 0x01
  xTaskCreate(vDebugTask, "DEBUG",
              configMINIMAL_STACK_SIZE,
              NULL,
              tskIDLE_PRIORITY + 1,
              NULL);
#endif
/*
  xTaskCreate(vHeightControlTask, "HCTRL",
              configMINIMAL_STACK_SIZE,
              NULL,
              tskIDLE_PRIORITY + 2,
              NULL);
*/
  vTaskStartScheduler();
}

// empty main loop
void loop() { }

/*
   @callback receiverOne: interrupt handler routine
   @summary: measure the lenght of pulse on RX1 pin
   @see: RX1
*/
void receiverOne() {
    if (!rx1_flag) {
        rx1_timer = TIMER3_BASE->CCR1;
        TIMER3_BASE->CCER |= TIMER_CCER_CC1P;
        rx1_flag = true;
    }
    else{ //  
        rx1_pulse_width = TIMER3_BASE->CCR1 - rx1_timer;
        if (rx1_pulse_width < 0) rx1_pulse_width += 0xFFFF;
        TIMER3_BASE->CCER &= ~TIMER_CCER_CC1P;
        rx1_flag = false;
    }
}

/*
   @callback receiverTwo: interrupt handler routine
   @summary: measure the lenght of pulse on RX2 pin
   @see: RX2
*/
void receiverTwo() {
    if (!rx2_flag) {
        rx2_timer = TIMER3_BASE->CCR2;
        TIMER3_BASE->CCER |= TIMER_CCER_CC2P;
        rx2_flag = true;
    }
    else{ //  
        rx2_pulse_width = TIMER3_BASE->CCR2 - rx2_timer;
        if (rx2_pulse_width < 0) rx2_pulse_width += 0xFFFF;
        TIMER3_BASE->CCER &= ~TIMER_CCER_CC2P;
        rx2_flag = false;
    }
}
