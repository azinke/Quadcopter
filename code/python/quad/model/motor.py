""" Model a motor
    
    @author: AMOUSSOU Z. Kenneth
    @date: 13-02-2019
    @version:
"""
from sys import exit

class Motor:
    """ Motor modeling
        The aim is to be able to compute the voltage across the motor and the 
        current through it given its speed and torque
        :output:
            [1] voltage across the motor
            [2] current through the motor
    """
    
    def __init__(self, config: dict):
        """ Constructor
            :param config: configuration data of the motor
        """
        try:
            self._kv0 = config["KV-constant"]
            self._um0 = config["no-load-voltage"]
            self._im0 = config["no-load-current"]
            self._rm = config["armature-resistor"]
            # ignored for simplification
            # self._lm = config["armature-inductance"]
            self._imax = config["max-current"]
            self._w = config["weight"]
        except:
            print("Missing configuration data for modeling the motor!")
            exit(0)
    
    def getVoltage(self, speed: float, torque: float) -> float:
        """ compute the voltage across the motor given its speed and torque
            :param speed: the motor's speed in rpm (rotation per minute)
            :param torque: the motor's torque in Newton Meter [Nm]
            :return: the voltage across the motor
        """
        _buf = ((self._um0 - (self._im0*self._rm))/(self._kv0*self._um0))*speed
        return (self.getCurrent(torque) * self._rm + _buf)
    
    def getCurrent(self, torque: float) -> float:
        """ compute the current through the motor given its torque
            :param torque: the actual torque of the motor
            :return: the current through the motor
        """
        _buf = torque * self._kv0 * self._um0
        _buf = _buf/(9.55*(self._um0 - (self._im0*self._rm)))
        return (_buf + self._im0)

if __name__ == "__main__":
    # configuration of the motor
    config = {
        "KV-constant": 2400,        # rmp/volt
        "no-load-voltage": 11.1,    # volt
        "no-load-current": 0.6,     # ampere
        "armature-resistor": 0.01,  # ohm
        "max-current": 6.5,         # ampere
        "weight": 0                 # gram
    }
    # draft data for testing
    torque = 0.008
    speed = 10000
    motor = Motor(config)
    print("Voltage (speed: {0} rmp, torque: {1} Nm): {2} V".format(speed, 
                                    torque, motor.getVoltage(speed, torque)))
    print("Current (speed: {0} rmp, torque: {1} Nm): {2} A".format(speed, 
                                    torque, motor.getCurrent(torque)))
