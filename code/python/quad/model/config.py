""" Configurations
    General configuration file of the quadcopter
    @author: AMOUSSOU Z. Kenneth
    @date: 14-02-2019
"""

#
# System configurations
#

# Environment
env = {
    "temp": 32,                 # degre
    "pressure": 100551,         # pascal
    "height": 5,                # meter
    "gravity": 9.81             # meter per second squared (m/s²)
}

# Propeller configuration
prop_config = {
    "diameter": 0.127,          # meter
    "pitch": 0.1143,            # meter
    "nb-blade": 2,              # dimensonless
    "weight": 4.25              # gram
}

# Motor configuration
motor_config = {
    "KV-constant": 2400,        # rmp/volt
    "no-load-voltage": 11.1,    # volt
    "no-load-current": 0.6,     # ampere
    "armature-resistor": 0.05,  # ohm
    "max-current": 6.5,         # ampere
    "max-power": 70,            # watt
    "weight": 18                # gram
}

# ESC configuration
esc_config = {
    "max-current": 16,          # ampere
    "output-resistor": 0.008,   # ohm
    "weight": 6                 # gram
}

# Battery configuration
bat_config = {
    "capacity": 1800,           # milli ampere
    "internal-resistor": 0.12,  # ohm
    "voltage": 12.61,           # volt
    "c-rating": 30,             # dimensionless
    "weight": 144               # gram
}

# Quadcopter configuration
quad_config = {
    "weight": 444,              # gram
    "nb-propulsor": 4,          # dimensionless
    "iother": 0.5               # ampere
}
